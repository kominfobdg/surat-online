# SURAT ONLINE
Surat Online is a mobile messaging application

## Requirements
1. Node.js, download via [this link](https://nodejs.org/en/download/ "Node.js").
2. Expo cli, install with this command


    ``` bash
    $ npm install -g expo-cli
    ```
	
3. Open this project directory and type this to install all depedencies
    
    ``` bash
    $ npm install
    ```
4. Install [rn-pdf-reader-js](https://www.npmjs.com/package/rn-pdf-reader-js) with this command

    ``` bash
    $ npm i rn-pdf-reader-js
    ```
5. Install [react-native-datepicker](https://www.npmjs.com/package/react-native-datepicker) with this command

    ``` bash
    $ npm i react-native-datepicker
    ```

## Depedencies Version
1. Node.js : v10.15.3
2. npm : 6.4.1
3. expo-cli : 2.11.9
4. node_modules/rn-pdfreader-js : 0.2.1
5. node_modules/react-navigation : 3.3.2
6. node_modules/expo : 32.0.6
7. node_modules/react : 16.5.0
8. node_modules/react-native : 0.57.1
9. node_modules/react-native-elements : 1.1.0
10. node_modules/@expo/vector-icons : 9.0.0
11. node_modules/react-native-datepicker : 1.7.2

## Running
1. Open this project directory via command prompt or terminal, and then type this


    ``` bash
    $ expo start
    ```
	
2. After that a new browser will open and than wait untill barcode appear on bottom left of the screen
3. If you want to run it on your mobile, download expo on Google Playstore (android) or App Store (IPhone). And then scan that barcode
4. If you want to run it on IOS emulator, first download xcode and install emulator there. And then on the new browser opened in step 2, click run on IOS Simulator

## Deploy
To deploy this project to iTunes Store or Google Play Store, you can follow tutorial in [this link](https://docs.expo.io/versions/latest/distribution/app-stores/)