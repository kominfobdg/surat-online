import React, { Component } from 'react'
import {StyleSheet, View, Image, Button, Text, TextInput, TouchableOpacity} from 'react-native'


export default class LoginScreen extends React.Component {
    render(){
        return (
            <View style={styles.container}>
                <View style={styles.top}>
                    <Image
                        style={styles.logo}
                        source={require('../../assets/images/email.png')}
                    />
                    <Text style={styles.title}>SURAT ONLINE</Text>
                    <Text style={styles.desc}>Aplikasi Surat Online Pemkot Bandung</Text>
                </View>
                <View style={styles.bot}>
                </View>
                <View style={styles.loginbox}>
                    <Text style={{textAlign: 'center', fontSize: 11}}>Silahkan masukkan alamat email Bandung.go.id dan kata sandi untuk melanjutkan ke dalam aplikasi SURAT ONLINE</Text>
                    <View style={styles.inputContainer}>
                        <Image
                            source={require('../../assets/images/user.png')}
                            style={styles.icon}
                        />
                        <TextInput style={styles.input}
                            placeholder='email bandung.go.id'
                            placeholderTextColor = '#808080'
                        ></TextInput>
                    </View>
                    <View style={styles.inputContainer}>
                        <Image
                            source={require('../../assets/images/lockpassword.png')}
                            style={styles.icon}
                        />
                        <TextInput style={styles.input}
                            placeholder='kata sandi pengguna'
                            placeholderTextColor = '#808080'
                            secureTextEntry={true}
                        ></TextInput>
                    </View>
                    <TouchableOpacity
                        style={{marginTop: 10}}
                        onPress={() => this.props.navigation.navigate('Home') }>
                        <View style={styles.buttonLogin}>
                            <Text style={{color: 'white', fontWeight: 'bold'}}>MASUK KE APLIKASI</Text>
                        </View>
                    </TouchableOpacity>
                    <Button
            title="Go to Details"
            onPress={() => this.props.navigation.navigate('Home')}
          />
                    <Text style={{color: 'grey', fontSize: 11, textAlign: 'center'}}>Tidak punya akun? Silahkan Hubungi admin Surat Online Diskominfo Bandung</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'lightgrey',
        flex: 1
    },
    top: {
        backgroundColor: '#1976D2',
        flex: 1,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        paddingTop: '10%',
    },
    bot: {
        flex: 1
    },
    loginbox: {
        backgroundColor: 'white',
        borderRadius: 5,
        borderColor: 'grey',
        borderWidth: 1,
        position: 'absolute',
        height: 300,
        width: '95%',
        alignSelf: 'center',
        top: '35%',
        paddingTop: 10,
        paddingHorizontal: 10,
    },
    logo: {
        width: 120,
        height: 120,
        alignSelf: 'center',
        paddingTop: 10,
    },
    title: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 16,
        alignSelf: 'center'
    },
    desc: {
        color: 'white',
        fontWeight: 'bold',
        alignSelf: 'center'
    },
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        borderColor: 'lightgrey',
        borderWidth: 1.5,
        width: '100%',
        height: 50,
        backgroundColor: '#f0f0f0',
        marginVertical: 10,
    },  
    input : {
        flex: 1,
        fontWeight: 'bold'
    },
    icon: {
        padding: 10,
        margin: 5,
        marginLeft: 10,
        height: 27,
        width: 22,
        resizeMode: 'stretch',
        alignItems: 'center'
    },
    buttonLogin: {
        backgroundColor: '#4CAF50',
        alignItems: 'center',
        borderRadius: 50,
        height: 50,
        width: 200,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    }
});