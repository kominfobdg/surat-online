


function FixValue(v){
    if(v < 10){
        return '0'+v;
    }
    return v;
}



const Helper = {
    Date: function(date){
        if(date == null || date == ''){
            return '-';
        }
        let d = new Date(date.split('-').join('/'));
        let str = FixValue(d.getHours()) + ':' + FixValue(d.getMinutes()) + ' - ' + FixValue(d.getDate());
        str += this.MonthToString(d.getMonth());
        str += ' ' + d.getFullYear();
        return str;
    },
    DateOnly: function(date){
        if(date == null || date == ''){
            return '';
        }
        let d = new Date(date.split('-').join('/'));
        return FixValue(d.getDate()) + this.MonthToString(d.getMonth()) + ' ' + d.getFullYear();
    },
    MonthToString: function(month){
        str = '';
        switch(month){
            case 1:
                str += ' Jan';
                break;
            case 2:
                str += ' Feb';
                break;
            case 3:
                str += ' Mar';
                break;
            case 4:
                str += ' Apr';
                break;
            case 5:
                str += ' Mei';
                break;
            case 6:
                str += ' Jun';
                break;
            case 7:
                str += ' Jul';
                break;
            case 8:
                str += ' Agu';
                break;
            case 9:
                str += ' Sep';
                break;
            case 10:
                str += ' Okt';
                break;
            case 11:
                str += ' Nov';
                break;
            case 12:
                str += ' Des';
                break;
            default:
                str += ' ?';
                break;
        }
        return str;
    }
}

export default Helper;

