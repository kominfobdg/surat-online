const SERVER_URL = 'https://e-suratonline.bandung.go.id/dev/api/index.php/';
const SERVER_URL2 = 'https://e-suratonline.bandung.go.id/dev/index.php/';

const URL = {
    MASTER_DATA: SERVER_URL+'master_data',
    LOGIN: SERVER_URL+'login',
    SURAT_MASUK_LIST: SERVER_URL+'surat_masuk',
    SURAT_MASUK_DETAIL: SERVER_URL+'surat_masuk/detail',
    KIRIM_DISPOSISI: SERVER_URL+'surat_masuk/disposisi',
    BATALKAN_DOSPOSISI: SERVER_URL+'surat_masuk/delete_disposisi',
    KIRIM_LAPORAN_KEGIATAN: SERVER_URL+'surat_masuk/laporan',
    SET_KEHADIRAN: SERVER_URL+'surat_masuk/hadir',
    SURAT_KELUAR_LIST: SERVER_URL+'surat_keluar',
    SURAT_KELUAR_DETAIL: SERVER_URL+'surat_keluar/detail',
    KONSEP_SURAT_LIST: SERVER_URL+'konsep',
    KONSEP_SURAT_DETAIL: SERVER_URL2+'wv/konsep/update/',
    KONSEP_SURAT_DETAIL2: SERVER_URL+'konsep/detail',
    BUAT_KONSEP_SURAT: SERVER_URL2+'wv/konsep/insert/',
    AGENDA: SERVER_URL+'agenda',
    UBAH_PASSWORD: SERVER_URL+'akun/update',
    DISPOSISI_KONSEP_SURAT: SERVER_URL+'konsep/disposisi',
    VALIDASI: SERVER_URL+'konsep/validasi',
    KONSEP_KIRIM: SERVER_URL+'konsep/kirim'

}

const Server = {
    GetMasterData: async function(token){
        let formdata = new FormData();
        formdata.append('token', token);
        return fetch(URL.MASTER_DATA, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formdata
        })
        .then((response) => response.json())
    },
    Login: async function(email, password){
        let formdata = new FormData();
        formdata.append('email', email);
        formdata.append('password', password);
        formdata.append('regid', 'ios-999999');
        return fetch(URL.LOGIN, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formdata
        })
        .then((response) => response.json())
    },
    SuratMasukList: async function(token, search){
        let formdata = new FormData();
        formdata.append('token', token);
        formdata.append('page', '1');
        formdata.append('kata_kunci', search == null ? '' : search );
        return fetch(URL.SURAT_MASUK_LIST, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formdata
        })
        .then((response) => response.json())
    },
    SuratMasukDetail: async function(token, id){
        let formdata = new FormData();
        formdata.append('token', token);
        formdata.append('id', id);
        return fetch(URL.SURAT_MASUK_DETAIL, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formdata
        })
        .then((response) => response.json())
    },
    KirimDisposisi: async function(token, id, id_user, catatan){
        let formdata = new FormData();
        formdata.append('token', token);
        formdata.append('id', id);
        formdata.append('id_user', JSON.stringify(id_user));
        formdata.append('catatan', catatan);
        return fetch(URL.KIRIM_DISPOSISI, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formdata
        })
        .then((response) => response.json())
    },
    BatalkanDisposisi: async function(token, id){
        let formdata = new FormData();
        formdata.append('token', token);
        formdata.append('id', id);
        return fetch(URL.BATALKAN_DOSPOSISI, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formdata
        })
        .then((response) => response.json())
    },
    SetKehadiran: async function(token, id, hadir){
        let formdata = new FormData();
        formdata.append('token', token);
        formdata.append('id', id);
        formdata.append('status_kehadiran', hadir ? 1 : 0);
        return fetch(URL.SET_KEHADIRAN, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formdata
        })
        .then((response) => response.json())
    },
    KirimLaporan: async function(token, id, pembicara, judul, ringkasan, photo){
        let formdata = new FormData();
        formdata.append('token', token);
        formdata.append('id', id);
        formdata.append('judul', judul);
        formdata.append('pembicara', pembicara);
        formdata.append('ringkasan_kegiatan', ringkasan);
        let op = {uri: photo.uri, name: 'img.jpg', type: 'image/jpg'};
        console.log(op);
        formdata.append('foto[]', op);
        return fetch(URL.KIRIM_LAPORAN_KEGIATAN, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formdata
        })
        .then((response) => response.json())
    },
    SuratKeluarList: async function(token, search){
        let formdata = new FormData();
        formdata.append('token', token);
        formdata.append('page', '1');
        formdata.append('kata_kunci', search == null ? '' : search );
        return fetch(URL.SURAT_KELUAR_LIST, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formdata
        })
        .then((response) => response.json())
    },
    SuratKeluarDetail: async function(token, id){
        let formdata = new FormData();
        formdata.append('token', token);
        formdata.append('id', id);
        return fetch(URL.SURAT_KELUAR_DETAIL, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formdata
        })
        .then((response) => response.json())
    },
    KonsepSuratList: async function(token, search){
        let formdata = new FormData();
        formdata.append('token', token);
        formdata.append('page', '1');
        formdata.append('kata_kunci', search == null ? '' : search );
        return fetch(URL.KONSEP_SURAT_LIST, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formdata
        })
        .then((response) => response.json())
    },
    KonsepSuratDetail: function(token, id){
       return URL.KONSEP_SURAT_DETAIL + token + '/' + id;
    },
    KonsepSuratDetail2: async function(token, id){
        let formdata = new FormData();
        formdata.append('token', token);
        formdata.append('id', id);
        return fetch(URL.KONSEP_SURAT_DETAIL2, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formdata
        })
        .then((response) => response.json())
    },
    BuatKonsepSurat: function(token){
        return URL.BUAT_KONSEP_SURAT + token;
    },
    Agenda: async function(token, startDate, endDate){
        let formdata = new FormData();
        formdata.append('token', token);
        formdata.append('start', startDate);
        formdata.append('end', endDate);
        return fetch(URL.AGENDA, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formdata
        })
        .then((response) => response.json())
    },
    UbahPassword: async function(token, password, password2){
        let formdata = new FormData();
        formdata.append('token', token);
        formdata.append('password', password);
        formdata.append('ulangi_password', password2);
        return fetch(URL.UBAH_PASSWORD, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formdata
        })
        .then((response) => response.json())
    },
    DisposisiKonsepSurat: async function(token, id, id_user, catatan){
        let formdata = new FormData();
        formdata.append('token', token);
        formdata.append('id', id);
        formdata.append('id_user', id_user);
        formdata.append('catatan', catatan);
        return fetch(URL.DISPOSISI_KONSEP_SURAT, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formdata
        })
        .then((response) => response.json())
    },
    Validasi: async function(token, id, siap_kirim, pin){
        let formdata = new FormData();
        formdata.append('token', token);
        formdata.append('id', id);
        formdata.append('siap_kirim', siap_kirim);
        formdata.append('pin', pin);
        return fetch(URL.VALIDASI, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formdata
        })
        .then((response) => response.json())
    },
    KonsepKirim: async function(token, id){
        let formdata = new FormData();
        formdata.append('token', token);
        formdata.append('id', id);
        return fetch(URL.KONSEP_KIRIM, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formdata
        })
        .then((response) => response.json())
    }

}

export default Server;