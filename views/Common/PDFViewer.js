import React from 'react';
import PDFReader from 'rn-pdf-reader-js';
import { View, WebView, Text } from 'react-native';
import { Constants } from 'expo';

export default class PDFViewer extends React.Component {

    static navigationOptions = {
        title: 'Pertinjau'
    };
    render() {
        let { url } = this.props;
        if(url == null || url == ''){
            url = this.props.navigation.getParam('url');
        }
        
        return(
            <View style={{flex: 1}}>
                {url == '' || url == null ? <Text>Tidak ada data</Text> : (url.substring(url.length - 3, url.length) == 'pdf' ? <PDFReader source={{ uri: url }} /> : <WebView source={{uri: url}} />) }
            </View>
        )
    }
}