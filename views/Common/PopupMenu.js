import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, UIManager, findNodeHandle  } from "react-native";
import { Ionicons } from '@expo/vector-icons';

export default class PopupMenu extends React.Component {
    static propTypes = {
      actions: PropTypes.arrayOf(PropTypes.string).isRequired,
      onPress: PropTypes.func.isRequired
      
    }

    constructor(props){
      super(props);
      this.state = {
        icon: null
      }
    }

    onError () {
      console.lof("Popup error");
    }

    onPress = () => {
      if(this.state.icon){
        UIManager.showPopupMenu(
          findNodeHandle(this.state.icon),
          this.props.actions,
          this.props.onError,
          this.props.onPress
        )
      }
    }

    render() {
      return(
        <View>
          <TouchableOpacity onPress={this.onPress} style={{width: 25}}>
            <Ionicons name='md-more' size={25} color='white' ref={this.onRef} style={{alignSelf: 'center'}}/>
          </TouchableOpacity>
        </View>
      );
    }

    onRef = icon => {
      if(!this.state.icon){
        this.setState({icon})
      }
    }
  }