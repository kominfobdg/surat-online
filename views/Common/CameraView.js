import React from 'react';
import { Text, View, TouchableOpacity, Image, ActivityIndicator, StyleSheet, Modal } from 'react-native';
import { Camera, Permissions, ImagePicker } from 'expo';

export default class CameraView extends React.Component {
  state = {
    hasCameraPermission: null,
    type: Camera.Constants.Type.back,
    photo: null ,
    isLoading: false
  };

  openLoading = () => {
    this.setState({ isLoading: true});
  }

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted'});
  }

  snap = async () => {
    //this.setState({isLoading: true})
    if(this.camera) {
      //this.setState({isLoading: true})
      let photo = await this.camera.takePictureAsync({
        //base64: true
      });
      this.setState({
        photo: photo
      });
      this.props.navigation.state.params.setPhoto(photo);
      this.props.navigation.goBack();
    } else {
      alert('Camera error');
      this.props.navigation.goBack();
    }
  }

  pickImage = async () => {
    let image = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: 'Images',
      //base64: true
    });
    if(!image.cancelled){
      this.props.navigation.state.params.setPhoto(image);
      this.props.navigation.goBack();
    }
  }

  render() {
    
    const { hasCameraPermission } = this.state;
    
    if (hasCameraPermission === null) {
      return <View />;
    } else if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Camera style={styles.camera} type={this.state.type} ref={ref => { this.camera = ref; }}>
          {this.state.isLoading == false ?
            (<View style={styles.container}>
               <TouchableOpacity
                style={styles.flip}
                onPress={() => {
                  this.setState({
                    type: this.state.type === Camera.Constants.Type.back
                      ? Camera.Constants.Type.front
                      : Camera.Constants.Type.back,
                  });
                }}>
                <Image
                  style={{ alignSelf: 'center', height: 35, width: 35 }} source={require('../../assets/images/flip.png')}>
                </Image>
              </TouchableOpacity>
              
              </View>) : <View></View> }
              <View style={{flex: 1, justifyContent: 'center'}}>
            {this.state.isLoading ? <Text style={{color: 'white', fontSize: 20, fontWeight:'bold', alignSelf: 'center'}}>Loading</Text> : <Text></Text>}
            </View>
              {this.state.isLoading == false ?
            (<View>
              <TouchableOpacity
                  style={styles.snap}
                  onPress={() => this.snap()}
                  >
              </TouchableOpacity>
              <TouchableOpacity
                  style={styles.pick}
                  onPress={() => this.pickImage()}
                  >
              </TouchableOpacity>
            </View>) : <View></View> }
            
          </Camera>
        </View>
      );
    }
  }

  
}

const CustomProgressBar = ({ visible }) => (
  <Modal onRequestClose={() => null} visible={visible}>
    <View style={{flex: 1, backgroundColor: '#dcdcdc', alignItems: 'center', justifyContent: 'center'}}>
      <View style={{borderRadius: 10, borderColor: 'white', padding: 25}}>
        <Text style={{ fontSize: 20, fontWeight: '200'}}>Loading</Text>
        <ActivityIndicator size='large' />
      </View>
    </View>
  </Modal>  
);

const styles = StyleSheet.create({
  camera: {
    flex: 1
  },
  cameraLoading: {
    flex: 1,
    justifyContent: 'center'
  },
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  flip: {
    backgroundColor:'#fff',
    borderRadius: 100,
    height: 50,
    width: 50,  
    margin: 10,
    justifyContent: 'center'
  },
  snap: {
    backgroundColor:'#fff',
    borderRadius: 100,
    height: 65,
    width: 65,
    alignSelf: 'center',  
    marginBottom: 10,
  },
  pick : {
    backgroundColor:'#ff0000',
    borderRadius: 100,
    height: 65,
    width: 65,
    alignSelf: 'flex-start',  
    marginBottom: 10,
  }
})