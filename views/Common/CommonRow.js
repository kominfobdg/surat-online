import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
const CommonRow = ({title, data, isTitle=false }) => (
    <View style={styles.list}>
        <TouchableOpacity>
            <View style={styles.container}>
                <View style={styles.icon}>
                    <Text style={{fontSize: 20, fontWeight:'bold', color: 'white'}}>{ isTitle ? (title != null ? title[0].toUpperCase() : 'A') : (data != null ? data[0].toUpperCase() : 'A') }</Text>
                </View>
                <View style={{flex: 1, marginLeft: 10, marginBottom: 15}}>
                    <Text style={styles.title}>{title}</Text>
                    <Text style={styles.pengirim}>{data}</Text>
                </View>
                
            </View>
        </TouchableOpacity>
    </View>
)

const styles = StyleSheet.create({
    list: {
        flex: 1,
        borderWidth: 1,
        borderBottomColor: '#80808030',
        borderTopColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        margin: 10,
    },
    container: {
        flex: 1,
        flexDirection: 'row',
    },  
    read: {
        width: 24,
        height: 24,
        paddingBottom: 10,
        marginLeft: 5,
    },
    icon: {
        borderRadius: 50,
        backgroundColor: 'rgb(36,183,123)',
        width: 50,
        height: 50,
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        //color: '#80808080',
        fontSize: 16,
        fontWeight: 'bold'
    },
    pengirim: {
        color: '#0000005f',
        fontSize: 16
    },
    tujuan: {
        fontSize: 16
    },
    waktu: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#0000005f',
        alignSelf: 'center',
    },
    button: {
        backgroundColor: 'rgb(254, 89, 62)',
        borderRadius: 25,
        height: 50,
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',

    },
    btn_text: {
        color: 'white',
        fontWeight: 'bold',
    },
    footer: {
        height: 65,
        backgroundColor: 'white',
        justifyContent: 'center'
    },
})

export default CommonRow;