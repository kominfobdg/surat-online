import React, { Component } from 'react';
import {StyleSheet, View, Image, WebView, Text, TextInput, TouchableOpacity, ScrollView} from 'react-native';
import Helper from '../Common/Helper';
import { SQLite } from 'expo'

const db = SQLite.openDatabase('akun.db');
export default class Profile extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            nama: null,
            nip: null,
            pangkat: null,
            telp: null,
            email: null,
            foto: null,
            id_role: null
        }

        db.transaction(
            tx => {
                tx.executeSql("SELECT * FROM akun", [], (_,{rows}) => {
                    if(rows.length > 0){
                        this.setState({
                            nama: rows._array[0].nama,
                            nip: rows._array[0].nip,
                            pangkat: rows._array[0].pangkat,
                            telp: rows._array[0].telp,
                            email: rows._array[0].email,
                            foto: rows._array[0].foto,
                            id_role: rows._array[0].id_role,
                            
                        })
                    }
                })
            }
        )
    }

    static navigationOptions = {
        title: 'Profile'
    }

    render() {
       
        return (
            <View style={{flex: 1}}>
                <View style={{flex: 1}}>
                    <View style={styles.top}>
                       
                    </View>
                    <View style={styles.center}>
                        
                    </View>
                    <View style={styles.content}>
                        <View style={styles.box}>
                            <View style={{flexDirection: 'row', marginBottom: 10}}>
                                <View style={styles.icon}>
                                    <Image source={{uri: this.state.foto}} style={styles.image} />
                                </View>
                                <View style={{flex: 1, marginLeft: 10}}>
                                    <Text style={styles.title}>{this.state.nama}</Text>
                                    <Text style={styles.waktu}>{this.state.nip}</Text>
                                </View>
                            </View>
                            <Text style={styles.waktu}>Nomor Telepon</Text>
                            <Text style={styles.tt}>{this.state.telp}</Text>
                            <Text style={styles.waktu}>Email</Text>
                            <Text style={styles.tt}>{this.state.email}</Text>
                            <Text style={styles.waktu}>Terakhir Diubah</Text>
                            <Text style={styles.tt}>{this.state.pangkat}</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.footer}>
                    <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate('GantiPassword')}>
                        <View>
                            <Text style={styles.btn_text}>UBAH PASSWORD</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            
        )
    }
}


const styles = StyleSheet.create({
    image: {
        height: 30,
        width: 30,
        alignSelf: 'center'
    },
    perihal: {
        color: 'white',
        marginVertical: 10,
        fontSize: 14,
        fontWeight: 'bold'
    },
    content: {
        position: 'absolute',
        minHeight: 300,
        width: '90%',
        alignSelf: 'center',
        marginTop: 30
    },
    box: {
        flex: 1,
        backgroundColor: 'white',
        borderRadius: 10,
        padding: 10,
    },
    top: {
        flex: 1,
        backgroundColor: '#1976D2',
    },
    center: {
        flex: 1,
        backgroundColor: 'rgb(242, 241, 242)'
    },
    list: {
        flex: 1,
        borderWidth: 1,
        borderBottomColor: '#80808030',
        borderTopColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        margin: 10,
    },
    container: {
        flex: 1,
        flexDirection: 'row',
    },
    read: {
        width: 24,
        height: 24,
        paddingBottom: 10,
        marginLeft: 5,
    },
    icon: {
        borderRadius: 50,
        backgroundColor: '#80808030',
        width: 50,
        height: 50,
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 14,
        fontWeight: '500'
    },
    pengirim: {
        color: 'rgb(255,87,51)',
        fontSize: 16
    },
    tt: {
        fontSize: 14,
        marginBottom: 10
    },
    waktu: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#0000005f',
    },
    button: {
        backgroundColor: 'rgb(254, 89, 62)',
        borderRadius: 25,
        height: 50,
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',

    },
    btn_text: {
        color: 'white',
        fontWeight: 'bold',
    },
    footer: {
        height: 65,
        backgroundColor: 'white',
        justifyContent: 'center'
    },
})