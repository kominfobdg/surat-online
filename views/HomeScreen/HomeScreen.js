import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, UIManager, findNodeHandle, Alert  } from "react-native";
import { createAppContainer, createMaterialTopTabNavigator } from 'react-navigation';
import { SQLite, Util } from 'expo';
import { Ionicons } from '@expo/vector-icons';
import SuratMasukList from '../SuratMasukScreen/SuratMasukList';
import SuratKeluarList from '../SuratKeluarScreen/SuratKeluarList';
import KonsepSuratList from "../KonsepSuratScreen/KonsepSuratList";
import Server from "../Common/Server";

const db = SQLite.openDatabase('akun.db');

const HomeTabs = createMaterialTopTabNavigator(
    {
      SuratMasuk: {
        screen: ({screenProps, navigation}) => <SuratMasukList badge={screenProps} nav={navigation} />,
        navigationOptions: ({ screenProps, navigation }) => ({
          title: 'Surat Masuk',
          tabBarIcon: () => <TabWithBadge badgeCount={screenProps.badge.SuratMasukCount} /> 
        })
      },
      SuratKeluar: {
        screen: ({screenProps, navigation}) => <SuratKeluarList badge={screenProps} nav={navigation} />,
        navigationOptions: ({screenProps, navigation }) => ({
          title: 'Surat Keluar',
          tabBarIcon: () => <TabWithBadge badgeCount={screenProps.badge.SuratKeluarCount} /> 
        })
      },
      KonsepSurat: {
        screen: ({screenProps, navigation}) => <KonsepSuratList badge={screenProps} nav={navigation} />,
        navigationOptions: ({screenProps, navigation }) => ({
          title: 'Konsep Surat',
          tabBarIcon: () => <TabWithBadge badgeCount={screenProps.badge.KonsepSuratCount} /> 
          
        })
      },
    },
    {
      tabBarOptions: {
        labelStyle: {
          fontSize: 14,
          fontWeight: 'bold'
        },
        style: {
          backgroundColor: '#1976D2'
        },
        upperCaseLabel: false,
        scrollEnabled: true,
        showIcon: true
      }
    },
    
  );
  
  //export default createAppContainer(HomeTabs);
  
class TabWithBadge extends React.Component {
    render() {
      const { badgeCount } = this.props;
      return (
        <View style={{ margin: 5, flex: 1, justifyContent: 'center', flexDirection: 'row' }}>
          {badgeCount >= 0 && (
            
            <View
              style={{
                backgroundColor: 'orange',
                borderRadius: 15,
                height: 20,
                width: 20,
                marginTop: 5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{ color: 'white', fontSize: 10, fontWeight: 'bold' }}>
                {badgeCount}
              </Text>
              
            </View>
          )}
        </View>
      );
    }
  }
  
  const AppContainer = createAppContainer(HomeTabs);

  

  class Header extends React.Component {
      render() {
          const {homeNav} = this.props;
          return (
            
            <View style={styles.header}>
                <View>
                    <Text style={styles.title}>SURAT ONLINE</Text>
                    <Text style={{color: 'white'}}>Aplikasi Surat Online Bandung</Text>
                </View>
                <View style={styles.icons}>
                    <TouchableOpacity style={styles.icon}>
                        <Ionicons name='md-refresh' size={25} color='white' onPress={() => this.props.main_refresh()} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.icon} onPress={() => homeNav.navigation.navigate('Kalender')}>
                        <Ionicons name='md-calendar' size={25} color='white' />
                    </TouchableOpacity>
                    {/* <TouchableOpacity style={styles.icon}>
                        <Ionicons name='md-more' size={25} color='white' />
                    </TouchableOpacity> */}
                    {/* <PopupMenu style={styles.icon} actions={['Profile', 'Logout']} onPress={this.onPopupEvent}/> */}
                    <TouchableOpacity style={styles.icon} onPress={() => Alert.alert('Menu', 'Pilih menu', [
                      {text: 'Buat Konsep Surat', onPress: () => {
                        homeNav.navigation.navigate('BuatKonsepSurat')
                      }},
                      {text: 'Pencarian', onPress: () => {
                        homeNav.navigation.navigate('SearchScreen')
                      }},
                      {text: 'Profile', onPress: () => {
                        homeNav.navigation.navigate('Profile')
                      }},
                      {text: 'Logout', onPress: () => {
                        db.transaction(
                          tx => {
                              tx.executeSql('DELETE FROM akun');
                              Expo.Updates.reload();
                          }
                        )
                      }},
                      {text: 'Kembali'},
                    ])}>
                        <Ionicons name='md-more' size={25} color='white' />
                    </TouchableOpacity>
                </View>
            </View>
            
          );
      }
  }
var refreshKonsep = () => {}
var refreshMasuk = () => {}
var refreshKeluar = () => {}

export default class HomeScreen extends React.Component {
    constructor(props){
      super(props);
      this.state = {
        SuratMasukCount: 0,
        SuratKeluarCount: 0,
        KonsepSuratCount: 0,
        key: 0
      }
    }
    componentDidMount(){
      this.props.navigation.setParams({main_refresh: this.refresh});
      db.transaction(
        tx => {
          tx.executeSql('SELECT token FROM akun', [], (_, {rows}) => {
            if(rows.length >= 1){
                let token = rows._array[0].token;
                Server.GetMasterData(token).then(
                    (responseData) => {
                        if(responseData.status == false){
                            alert(responseData.message);
                            Expo.Updates.reload();
                        }
                    }
                )
                .catch((err) => {
                    alert(err);
                })
            } else {
              alert('Tidak ada data login');
              Expo.Updates.reload();
            }
          })
        }
      )
    }
    static navigationOptions = (navigation) => {
        //title: 'SURAT ONLINE',
        //console.log(navigation);
        return {
          headerTitle: <Header homeNav={navigation} main_refresh={navigation.navigation.getParam('main_refresh')}/>
        }
        
    }

    

    refresh = () => {
      // this.setState({key: Math.random(), SuratMasukCount: 0})
      //alert(Math.random());
      // console.log(this.refs);
      refreshKonsep();
      refreshKeluar()
      refreshKeluar()
    }
    
    setRefreshKonsep(refresh){
      refreshKonsep = refresh;
    }
    setRefreshMasuk(refresh){
      refreshMasuk = refresh;
    }
    setRefreshKeluar(refresh){
      refreshKeluar = refresh;
    }

    render(){
      console.log('home render');
      return (
        
        <View style={{flex: 1, backgroundColor: '#F2F2F2'}}>
          <AppContainer 
          screenProps={
              {
                  badge: this.state, 
                  updateMasuk: (count) => this.setState({SuratMasukCount: count}), 
                  updateKeluar: (count) => this.setState({SuratKeluarCount: count}), 
                  updateKonsep: (count) => this.setState({KonsepSuratCount: count}),
                  setRefreshKonsep: (refresh) => this.setRefreshKonsep(refresh),
                  setRefreshMasuk: (refresh) => this.setRefreshMasuk(refresh),
                  setRefreshKeluar: (refresh) => this.setRefreshKeluar(refresh),
                  homeNav: this.props.navigation
              }
            }
            ref='child'
            />
        </View>
        
      );
    }
  }

  const styles = StyleSheet.create({
    header: {
      flex: 1,
      height: 80,
      backgroundColor: '#1976D2',
      paddingTop: 30,
      paddingHorizontal: 15,
      flexDirection: 'row',
    },
    title: {
      color: 'white',
      fontWeight: 'bold',
      fontSize: 16
    },
    icons: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginLeft: 20,
      
    },
    icon: {
      marginHorizontal: 5,
      width: 25
    }
  });
  