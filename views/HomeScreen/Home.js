import { createAppContainer, createMaterialTopTabNavigator, createStackNavigator } from 'react-navigation';
import SuratMasukDetail from '../SuratMasukScreen/SuratMasukDetail';
import SuratKeluarDetail from '../SuratKeluarScreen/SuratKeluarDetail';
import HomeScreen from './HomeScreen';
import CameraView from '../Common/CameraView';
import Disposisi from '../SuratMasukScreen/Disposisi/Disposisi';
import KonsepSuratDetail from '../KonsepSuratScreen/KonsepSuratDetail';
import TujuanPerangkatDaerah from '../KonsepSuratScreen/KonsepSurat/TujuanPerangkatDaerah';
import TujuanLainya from '../KonsepSuratScreen/KonsepSurat/TujuanLainya';
import Tembusan from '../KonsepSuratScreen/KonsepSurat/Tembusan';
import IsiSurat from '../KonsepSuratScreen/KonsepSurat/IsiSurat';
import PDFViewer from '../Common/PDFViewer';
import BuatKonsepSurat from '../BuatKonsepSurat/BuatKonsepSurat';
import SearchSreen from '../SearchScreen/SearchScreen';
import Kalender from '../Kalender/Kalender';
import AgendaScreen from '../Kalender/Agenda';
import Profile from '../Profile/Profile';
import GantiPassword from '../Profile/GantiPassword';
import Validasi from '../KonsepSuratScreen/KonsepSurat/Validasi';
import TeruskanKonsepSurat from '../KonsepSuratScreen/TeruskanKonsepSurat/TeruskanKonsepSurat';


const HomeNavigator = createStackNavigator(
  {
    Home: {
      screen: HomeScreen
    },
    SuratMasuk: {
      screen: SuratMasukDetail
    },
    SuratKeluar: {
      screen: SuratKeluarDetail
    },
    Disposisi: {
      screen: Disposisi
    },
    TujuanPerangkatDaerah: {
      screen: TujuanPerangkatDaerah
    },
    TujuanLainya: {
      screen: TujuanLainya
    },
    Tembusan: {
      screen: Tembusan
    },
    IsiSuratKonsep: {
      screen: IsiSurat
    },
    KonsepSurat: {
      screen: KonsepSuratDetail
    },
    CameraView: {
      screen: CameraView
    },
    PDFViewer: {
      screen: PDFViewer
    },
    BuatKonsepSurat: {
      screen: BuatKonsepSurat
    },
    SearchScreen: {
      screen: SearchSreen
    },
    Kalender: {
      screen: AgendaScreen
    },
    Profile: {
      screen: Profile
    },
    GantiPassword: {
      screen: GantiPassword
    },
    IsiKonsepSurat: {
      screen: IsiSurat
    },
    FormValidasi: {
      screen: Validasi
    },
    TeruskanKonsep: {
      screen: TeruskanKonsepSurat
    }
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#1976D2'
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
          fontSize: 16,
          fontWeight: 'bold'
      }
    }
  }
);

const Home = createAppContainer(HomeNavigator);


export default Home;

