import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import {Agenda} from 'react-native-calendars';
import Server from '../Common/Server';
import {SQLite} from 'expo';

const db = SQLite.openDatabase('akun.db');

export default class AgendaScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: {},
      startDate: new Date(),
      token: null
    };
    db.transaction(
        tx => {
            tx.executeSql("SELECT token FROM akun", [], (_,{rows}) => {
                if(rows.length > 0){
                    let token = rows._array[0].token;
                    this.setState({
                        token: token
                    })
                }
            })
        }
    )
  }

  static navigationOptions = {
    title: 'Agenda'
};

  render() {

    return (
      <Agenda
        items={this.state.items}
        loadItemsForMonth={this.loadItems.bind(this)}
        selected={this.state.startDate}
        renderItem={this.renderItem.bind(this)}
        renderEmptyDate={this.renderEmptyDate.bind(this)}
        rowHasChanged={this.rowHasChanged.bind(this)}
        // markingType={'period'}
        // markedDates={{
        //    '2017-05-08': {textColor: '#666'},
        //    '2017-05-09': {textColor: '#666'},
        //    '2017-05-14': {startingDay: true, endingDay: true, color: 'blue'},
        //    '2017-05-21': {startingDay: true, color: 'blue'},
        //    '2017-05-22': {endingDay: true, color: 'gray'},
        //    '2017-05-24': {startingDay: true, color: 'gray'},
        //    '2017-05-25': {color: 'gray'},
        //    '2017-05-26': {endingDay: true, color: 'gray'}}}
         // monthFormat={'yyyy'}
         // theme={{calendarBackground: 'red', agendaKnobColor: 'green'}}
        //renderDay={(day, item) => (<Text>{day ? day.day: 'item'}</Text>)}
      />
    );
  }

  dateCheck(from,to,check) {

    var fDate,lDate,cDate;
    fDate = Date.parse(from);
    lDate = Date.parse(to);
    cDate = Date.parse(check);

    if((cDate <= lDate && cDate >= fDate)) {
        return true;
    }
    return false;
}

    loadItems(day) {
        if(this.state.token == null){
            return;
        }
        let startDate = this.timeToString(day.timestamp - 30 * 24 * 60 * 60 * 1000); //60 day before
        let endDate = this.timeToString(day.timestamp + 30 * 24 * 60 * 60 * 1000); //84 day after
        console.log(startDate);
        console.log(endDate);
        // endDate = day.year + '-' + day.month + '-' + endDate.getDate();
        // //console.log(day);
        Server.Agenda(this.state.token, startDate, endDate).then(
          data => {
            for (let i = -85; i < 85; i++) {
              const time = day.timestamp + i * 24 * 60 * 60 * 1000;
              const strTime = this.timeToString(time);
              //if (!this.state.items[strTime]) {
                this.state.items[strTime] = [];
              //}
            }
            //console.log(this.state.items);
            data.map((value, id) => {
              //console.log(value.tanggal_mulai);
              if(value.tanggal_mulai != null){
                if(value.tanggal_selesai == null){
                  value.tanggal_selesai = value.tanggal_mulai
                }
                
                
                let d1 = new Date(value.tanggal_mulai);
                let d2 = new Date(value.tanggal_selesai);
                let diff = parseInt((d2-d1)/(24*3600*1000));
                let currDate = d1.getTime();
                for(let i = 0; i<=diff; i++){
                  const time = currDate + i * 24 * 60 * 60 * 1000;
                  const strTime = this.timeToString(time);
                  if(!this.state.items[strTime]){
                    this.state.items[strTime] = [];
                  }
                  this.state.items[strTime].push(
                    {name: value.perihal, height: 50, id: value.id, timeStart: value.jam_mulai, timeEnd: value.jam_selesai, tipe: value.tipe_surat}
                  )
                }
              }
              
              
            });
            const newItems = {};
            Object.keys(this.state.items).forEach(key => {newItems[key] = this.state.items[key];});
            //console.log(newItems);
            this.setState({
                items: newItems
            });
          }
        ).catch(err => alert(err));
        // setTimeout(() => {
        //   for (let i = -15; i < 85; i++) {
        //       const time = day.timestamp + i * 24 * 60 * 60 * 1000;
        //       const strTime = this.timeToString(time);
        //       if (!this.state.items[strTime]) {
        //         //console.log(strTime);
        //         this.state.items[strTime] = [];
        //         const numItems = Math.floor(Math.random() * 5);
        //         for (let j = 0; j < numItems; j++) {
        //             this.state.items[strTime].push({
        //             name: 'Item for ' + strTime,
        //             height: Math.max(50, Math.floor(Math.random() * 150))
        //             });
        //         }
        //       }
        //   }
        //   //console.log(this.state.items);
        //   const newItems = {};
        //   Object.keys(this.state.items).forEach(key => {newItems[key] = this.state.items[key];});
        //   //console.log(newItems);
        //   this.setState({
        //       items: newItems
        //   });
        // }, 1000);
        
        //console.log(`Load Items for ${day.year}-${day.month}`);
    }

  renderItem(item) {
    return (
      <TouchableOpacity onPress={() => {
        if(item.tipe == 'surat_masuk'){
          this.props.navigation.navigate('SuratMasuk', {id: item.id});
        } else if(item.tipe == 'surat_keluar'){
          this.props.navigation.navigate('SuratKeluar', {id: item.id});
        }
      }}>
        <View style={[styles.item]}>
          <Text>{item.name}</Text>
          <Text>{item.timeStart} - {item.timeEnd}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  renderEmptyDate() {
    return (
      <View style={styles.emptyDate}><Text>Tidak ada kegiatan</Text></View>
    );
  }

  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }

  timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17
  },
  emptyDate: {
    height: 15,
    flex:1,
    paddingTop: 30
  }
});