import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  ScrollView,
  View
} from 'react-native';
import {Calendar} from 'react-native-calendars';

export default class Kalender extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.onDayPress = this.onDayPress.bind(this);
  }

  render() {
    return (
        <View style={{flex: 1}}>
            <Calendar
            onDayPress={this.onDayPress}
            style={styles.calendar}
            // markedDates={{[this.state.selected]: {selected: true, disableTouchEvent: true, selectedDotColor: 'orange'}}}
            markedDates={
              {
                '2019-03-21': {
                  periods: [
                    { startingDay: true, endingDay: false, color: 'red'},
                  
                  ]
                },
                '2019-03-22': {
                  periods: [
                    { startingDay: false, endingDay: true, color: 'red'},
                  ]
                },
              }
            }
            markingType='multi-period'
            />
        </View>
    );
  }

  onDayPress(day) {
    this.setState({
      selected: day.dateString
    });
  }
}

const styles = StyleSheet.create({
  calendar: {
    borderTopWidth: 1,
    paddingTop: 5,
    borderBottomWidth: 1,
    borderColor: '#eee',
    height: 350
  },
  text: {
    textAlign: 'center',
    borderColor: '#bbb',
    padding: 10,
    backgroundColor: '#eee'
  },
  container: {
    flex: 1,
    backgroundColor: 'gray'
  }
});