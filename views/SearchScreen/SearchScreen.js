import React from 'react';
import {View, StyleSheet, Text, TextInput, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { createMaterialTopTabNavigator, createAppContainer } from 'react-navigation';
import SuratMasuk from '../SuratMasukScreen/SuratMasuk';
import SuratKeluarList from '../SuratKeluarScreen/SuratKeluarList';
import SuratMasukList from '../SuratMasukScreen/SuratMasukList';
import KonsepSuratList from '../KonsepSuratScreen/KonsepSuratList';

const SearchTab = createMaterialTopTabNavigator(
    {
        SuratMasuk: {
            screen:(props) => <SuratMasukList badge={null} search={props.screenProps.search} navigation={props.screenProps.homeNav} />,
            navigationOptions:  ({
                title: 'Surat Masuk',
            })
        },
        SuratKeluar: {
            screen:(props) => <SuratKeluarList badge={null} search={props.screenProps.search} navigation={props.screenProps.homeNav} />,
            navigationOptions:  ({
                title: 'Surat Keluar',
            })
        },
        KonsepSurat: {
            screen:(props) => <KonsepSuratList badge={null} search={props.screenProps.search} navigation={props.screenProps.homeNav} />,
            navigationOptions:  ({
                title: 'Konsep Surat',
            })
        }
    },
    {
        tabBarOptions: {
          labelStyle: {
            fontSize: 14,
            fontWeight: 'bold'
          },
          style: {
            backgroundColor: '#1976D2'
          },
          upperCaseLabel: false,
          scrollEnabled: true,
        }
    }
)

const AppContainer = createAppContainer(SearchTab);

export default class SearchSreen extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            search: null
        }
    }

    setSearch(text){
        this.setState({search: text})
        //console.log(text);
    }

    componentDidMount() {
        this.props.navigation.setParams({
            handleThis:(text) => this.setSearch(text)
        });
    }

    static navigationOptions = ({navigation}) => {
        const {params = {}} = navigation.state;
        let search = '';
        return{
            headerTitle: (
                <View style={{flexDirection: 'row'}}>
                    <TextInput
                        style={{
                            borderBottomColor: 'white',
                            borderBottomWidth: 1,
                            flex: 1,
                            color: 'white'
                        }}
                        // value={this.state.search}
                        onChangeText={(text) => params.handleThis(text)}
                        placeholder="Masukkan kata kunci pencarian"
                    />
                    <TouchableOpacity style={{
                        marginHorizontal: 10,
                        width: 25
                    }}
                    onPress={() => {

                    }}
                    >
                        <Ionicons name='md-search' size={25} color='white' />
                    </TouchableOpacity>
                </View>
            )
        }
    }
    
    render(){
        return(
            <AppContainer screenProps={{homeNav: this.props.navigation, search: this.state.search}}/>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    search_menu: {
        flexDirection: 'row'
    },
    search: {
        borderBottomColor: 'white',
        borderBottomWidth: 1
    },
    icon: {
        marginHorizontal: 5,
        width: 25
    }
})
