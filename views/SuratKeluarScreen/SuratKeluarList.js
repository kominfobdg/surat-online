import React, { Component } from 'react'
import {StyleSheet, View, Image, Button, Text, TextInput, TouchableOpacity, FlatList} from 'react-native'
import { SQLite } from 'expo';
import SuratKeluarListView from './SuratKeluarListView';
import Server from '../Common/Server';

const db = SQLite.openDatabase('akun.db');

export default class SuratKeluarList extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            data: [],
            search: null
        };
        if(this.props.badge != null){
            this.props.badge.setRefreshMasuk(() => this.refresh());
        }

    }

    refresh() {
        console.log('refreshing surat keluar');
        const search = this.props.search;
        db.transaction(
            tx => {
                tx.executeSql('SELECT token FROM akun', [], (_, {rows}) => {
                    if(rows.length >= 1){
                        let token = rows._array[0].token;
                        Server.SuratKeluarList(token, search)
                        .then((responseData) => {
                            if(responseData.status == false){
                                alert(responseData.message);
                            } else {
                                var count = 0;
                                for(var i = 0; i < responseData.data.length; i++){
                                    if(responseData.data[i].terbaca == '0/1'){
                                        count++;
                                    }
                                }
                                
                                if(this.props.badge != null){
                                    this.props.badge.updateKeluar(count);
                                }
                                this.setState({
                                    data: responseData.data
                                });
                                
                            }
                        })
                        .catch((err) => alert(err));
                    }
                });
            }
        );
    }

    componentDidUpdate(){
        const search = this.props.search;
        if(search != this.state.search){
            this.setState({
                search: search
            })
            this.refresh();
        }
    }

    componentDidMount(){
        this.refresh();
    }

    render() {
        if(this.props.badge != null){
            return (
                <SuratKeluarListView
                    itemlList={this.state.data}
                    homeNav={this.props.badge.homeNav}
                />
            )
        } else {
            return (
                <SuratKeluarListView
                    itemlList={this.state.data}
                    homeNav={this.props.navigation}
                />
            )
        }
    }

    
} 










