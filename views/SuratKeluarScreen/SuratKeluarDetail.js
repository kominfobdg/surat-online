import React from 'react';
import { StyleSheet, View, Text, TextInput } from 'react-native';
import { createMaterialTopTabNavigator, createAppContainer } from 'react-navigation';
import { SQLite } from 'expo';
import Server from '../Common/Server';
import IsiSurat from './IsiSurat/IsiSurat';
import StatusKirim from './StatusKirim/StatusKirim';
import Menghadiri from './Menghadiri/Menghadiri';


const db = SQLite.openDatabase('akun.db');

const SuratKeluarTab = createMaterialTopTabNavigator(
    {
        IsiSurat: {
            screen: (props) => <IsiSurat data={props.screenProps.data} navigation={props.screenProps.navigation} />,
            navigationOptions: {
                title: 'Isi Surat'
            }
        },
        StatusKirim: {
            screen: (props) => <StatusKirim data={props.screenProps.data} />,
            navigationOptions: {
                title: 'Status Kirim'
            },
        },
        Menghadiri: {
            screen: (props) => <Menghadiri data={props.screenProps.data} />,
            navigationOptions: {
                title: 'Menghadiri'
            },
        },
    },
    {
        tabBarOptions: {
            labelStyle: {
                fontSize: 14,
                fontWeight: 'bold'
            },
            style: {
                backgroundColor: '#1976D2'
            },
            upperCaseLabel: false,
            scrollEnabled: true
        }
    }
);

const SuratKeluarContainer = createAppContainer(SuratKeluarTab);

//export default SuratMasukScreen;

export default class SuratKeluarDetail extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            data: {
                surat: {
        
                },
                skpd_tujuan: [],
                kehadiran: [],
            }
        }
        //console.log(this.state.data);
    }

    
    static navigationOptions = {
        title: 'Detail Surat'
    };

    componentDidMount() {
        const id = this.props.navigation.getParam('id');
        db.transaction(
            tx => {
                tx.executeSql('SELECT token FROM akun', [], (_,{rows}) => {
                    if(rows.length >= 1){
                        Server.SuratKeluarDetail(rows._array[0].token, id).then(
                            (data) => {
                                if(data.status == false){
                                    alert(data.message);
                                } else {
                                    //console.log(data);
                                    this.setState({
                                        data: data.data
                                    })
                                }
                            }
                        );
                    }
 
                });    
            }
        );
    };
    
    render() {
        
        return (
            <View style={{flex: 1}}>
                <SuratKeluarContainer screenProps={{data: this.state.data, navigation: this.props.navigation}}/>
            </View>
           
        )
    }
}

