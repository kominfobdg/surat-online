import React from 'react';
import {StyleSheet, View, Image, Text, TouchableOpacity, FlatList} from 'react-native';
import Helper from '../../Common/Helper';

const StatusKirimRow = ({ nama_skpd, pengirim, tanggal, status_dibaca}) => (
    <View style={styles.list}>
        <TouchableOpacity>
            <View style={styles.container}>
                <View style={styles.icon}>
                    <Text style={{fontSize: 20, fontWeight:'bold', color: 'white'}}>A</Text>
                </View>
                <View style={{flex: 1, marginLeft: 10}}>
                    <Text style={styles.title}>Pengirim</Text>
                    <Text style={styles.pengirim}>{pengirim}</Text>
                    <Text style={styles.title}>Perangkat Daerah Tujuan</Text>
                    <Text style={styles.tujuan}>{nama_skpd}</Text>
                    <View style={{flex: 1, flexDirection: 'row', alignSelf: 'flex-end', marginBottom: 5}}>
                        <Text style={styles.waktu}>{Helper.Date(tanggal)}</Text>
                        {status_dibaca == null ? <Image style={styles.read} source={require('../../../assets/images/ic_unread.png')}/> : <Image style={styles.read} source={require('../../../assets/images/ic_read.png')}/>  }
                    </View>
                    
                </View>
                
            </View>
        </TouchableOpacity>
    </View>
);



const styles = StyleSheet.create({
    list: {
        flex: 1,
        borderWidth: 1,
        borderBottomColor: '#80808030',
        borderTopColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        margin: 10,
    },
    container: {
        flex: 1,
        flexDirection: 'row',
    },
    read: {
        width: 24,
        height: 24,
        paddingBottom: 10,
        marginLeft: 5,
    },
    icon: {
        borderRadius: 50,
        backgroundColor: 'rgb(36,183,123)',
        width: 50,
        height: 50,
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        //color: '#80808080',
        fontSize: 16,
        fontWeight: 'bold'
    },
    pengirim: {
        color: 'rgb(255,87,51)',
        fontSize: 16
    },
    tujuan: {
        fontSize: 16
    },
    waktu: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#0000005f',
        alignSelf: 'center',
    },
    button: {
        backgroundColor: 'rgb(254, 89, 62)',
        borderRadius: 25,
        height: 50,
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',

    },
    btn_text: {
        color: 'white',
        fontWeight: 'bold',
    },
    footer: {
        height: 65,
        backgroundColor: 'white',
        justifyContent: 'center'
    },
})


const StatusKirimListView = ({ itemList }) => (
    <View style={{flex: 1, backgroundColor: 'rgb(242, 241, 242)'}}>
        
        <FlatList
            data={itemList}
            renderItem={({item}) => <StatusKirimRow
                nama_skpd={item.nama_skpd}
                pengirim={item.pengirim}
                tanggal={item.tanggal}
                status_dibaca={item.status_dibaca}
            />}
            keyExtractor={(item, index) => index.toString()}
        />
    </View>
);
export default class StatusKirim extends React.Component {
    render() {
        const {data, navigation} = this.props;
        //console.log(data.list_disposisi);
        return (
            <View style={{flex: 1}}>
                <StatusKirimListView 
                    itemList={data.skpd_tujuan}
                />
            </View>
            
        )
    }
}

