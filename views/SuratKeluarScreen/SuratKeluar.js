import React, { Component } from 'react'
import {StyleSheet, View, Image, Button, Text, TextInput, TouchableOpacity, FlatList} from 'react-native'
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { SQLite } from 'expo';
import SuratKeluarList from './SuratKeluarList';
import SuratKeluarDetail from './SuratKeluarDetail';


const SuratKeluarNavigator = createStackNavigator({
    First: {
        screen: ({screenProps, navigation}) => <SuratKeluarList badge={screenProps} nav={navigation} />
    },
    SuratKeluar: SuratKeluarDetail
},
{
    headerMode: 'none',
    navigationOptions: {
        headerVisible: false
    }
});

const SuratKeluarNav = createAppContainer(SuratKeluarNavigator);

export default class SuratKeluar extends React.Component{
    constructor(props){
        super(props);
        //console.log(props.badge.screenProps);
        const { screenProps } = this.props.badge;
        //console.log(screenProps);
    }
    render() {
        const { screenProps } = this.props.badge;
        return (<SuratKeluarNav screenProps={{badge: screenProps}} />)
    }
}












