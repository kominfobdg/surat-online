import React from 'react';
import {View, WebView} from 'react-native';
import {SQLite} from 'expo';
import Server from '../Common/Server';

const db = SQLite.openDatabase('akun.db');

export default class BuatKonsepSurat extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            token: null
        }
        db.transaction(
            tx => {
                tx.executeSql("SELECT token FROM akun", [], (_,{rows}) => {
                    if(rows.length > 0){
                        let token = rows._array[0].token;
                        this.setState({
                            token: token
                        })
                    }
                })
            }
        )
    }

    static navigationOptions = {
        title: 'Buat Konsep Surat'
    };
    
    render(){
        return (
            <WebView
                source={{uri: Server.BuatKonsepSurat(this.state.token)}}
            />
        )
    }
}