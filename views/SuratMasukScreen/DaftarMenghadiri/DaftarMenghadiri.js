import React from 'react';
import {StyleSheet, View, Image, Text, TouchableOpacity, FlatList, Alert, ActivityIndicator} from 'react-native';
import CommonRow from '../../Common/CommonRow';
import {SQLite} from 'expo';
import Server from '../../Common/Server';
import Modal from 'react-native-modal';

const db = SQLite.openDatabase('akun.db');

const KehadiranListView = ({ itemList }) => (
    <View style={{flex: 1, backgroundColor: 'rgb(242, 241, 242)'}}>
        
        <FlatList
            data={itemList}
            renderItem={({item}) => <CommonRow
                title={item.peserta}
                data={item.skpd}
                isTitle={true}
            />}
            keyExtractor={(item, index) => index.toString()}
        />
    </View>
);


export default class DaftarMenghadiri extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isLoading: false
        }
    }
    setKehadiran = (id, hadir) => {
        this.setState({isLoading: true});
        db.transaction(
            tx => {
                tx.executeSql('SELECT token FROM akun', [], (_, {rows}) => {
                    Server.SetKehadiran(rows._array[0].token, id, hadir).then(
                        (responseData) => {
                            this.setState({isLoading: false});
                            if(responseData.status){
                                alert(responseData.message);
                            } else {
                                alert(responseData.message);
                            }
                        }
                    ).catch((err) => {this.setState({isLoading: true}); alert(err); });
                })
            }
        )
    }
    render() {
        const {data} = this.props;
        //console.log(data.list_disposisi);
        return (
            <View style={{flex: 1}}>
                <KehadiranListView 
                    itemList={data.kehadiran}
                />
                {data.can.set_kehadiran ? (
                <View style={styles.footer}>
                    <TouchableOpacity style={styles.button} onPress={() => Alert.alert('Set Kehadiran', 'Apakah anda akan hadir pada kegiatan ini?', [{text: 'Saya Hadir', onPress: () => this.setKehadiran(data.surat.id, true) }, {text: 'Saya Tidak Hadir', onPress: () => this.setKehadiran(data.surat.id, false)}])}>
                        <View>
                            <Text style={styles.btn_text}>SET KEHADIRAN</Text>
                        </View>
                    </TouchableOpacity>
                </View>)
                : <View/>
                }
                <Modal isVisible={this.state.isLoading} style={{marginVertical: 250, marginHorizontal: 50, backgroundColor: 'white', borderRadius: 5}}>
                    <View style={{ flex: 1, alignContent: 'center', justifyContent: 'center' }}>
                        <Text style={{fontSize: 16, color:'black', justifyContent: 'center', alignSelf: 'center'}}>Sedang Memproses</Text>
                        <ActivityIndicator
                            size='large'
                        />
                    </View>
                </Modal>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    button: {
        backgroundColor: 'rgb(254, 89, 62)',
        borderRadius: 25,
        height: 50,
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',

    },
    btn_text: {
        color: 'white',
        fontWeight: 'bold',
    },
    footer: {
        height: 65,
        backgroundColor: 'white',
        justifyContent: 'center'
    },
})
