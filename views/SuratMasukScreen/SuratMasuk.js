import React, { Component } from 'react'
import {StyleSheet, View, Image, Button, Text, TextInput, TouchableOpacity, FlatList} from 'react-native'
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { SQLite } from 'expo';
import SuratMasukListView from './SuratMasukListView';
import SuratMasukDetail from './SuratMasukDetail';
import SuratMasukList from './SuratMasukList';


const SuratMasukNavigator = createStackNavigator({
    First: {
        screen: ({screenProps, navigation}) => <SuratMasukList badge={screenProps} nav={navigation} />
    },
    SuratMasuk: SuratMasukDetail
},
{
    headerMode: 'none',
    navigationOptions: {
        headerVisible: false
    }
});

const SuratMasukNav = createAppContainer(SuratMasukNavigator);

export default class SuratMasuk extends React.Component{
    constructor(props){
        super(props);
        //console.log(props.badge.screenProps);
        const { screenProps } = this.props.badge;
        //console.log(screenProps);
    }
    render() {
        const screenProps = this.props.badge.screenProps;
        return (<SuratMasukNav screenProps={{badge: screenProps}} />)
        
    }
}












