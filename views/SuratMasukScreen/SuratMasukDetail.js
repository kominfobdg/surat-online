import React from 'react';
import { StyleSheet, View, Text, TextInput, TouchableOpacity } from 'react-native';
import { createMaterialTopTabNavigator, createAppContainer } from 'react-navigation';
import { SQLite } from 'expo';
import { Ionicons } from '@expo/vector-icons';
import Server from '../Common/Server';
import IsiSurat from './IsiSurat/IsiSurat';
import DisposisiList from './Disposisi/DisposisiList';
import LaporanKegiatan from './LaporanKegiatan/LaporanKegiatan';
import DaftarMenghadiri from './DaftarMenghadiri/DaftarMenghadiri';

const db = SQLite.openDatabase('akun.db');

const SuratMasukTab = createMaterialTopTabNavigator(
    {
        IsiSurat: {
            screen: (props) => <IsiSurat data={props.screenProps.data} navigation={props.screenProps.navigation} reload={props.screenProps}  />,
            navigationOptions: {
                title: 'Isi Surat'
            }
        },
        DaftarDisposisi: {
            screen: (props) => <DisposisiList data={props.screenProps.data} navigation={props.screenProps.navigation} reload={props.screenProps} />,
            navigationOptions: {
                title: 'Daftar Disposisi'
            },
        },
        LaporanKegiatan: {
            screen: (props) => <LaporanKegiatan data={props.screenProps.data} navigation={props.screenProps.navigation} reload={props.screenProps} />,
            navigationOptions: {
                title: 'Laporan Kegiatan'
            },
        },
        DaftarKehadiran: {
            screen: (props) => <DaftarMenghadiri data={props.screenProps.data} />,
            navigationOptions: {
                title: 'Daftar Kehadiran'
            },
        }
    },
    {
        tabBarOptions: {
            labelStyle: {
                fontSize: 14,
                fontWeight: 'bold'
            },
            style: {
                backgroundColor: '#1976D2'
            },
            upperCaseLabel: false,
            scrollEnabled: true
        }
    }
);

const SuratMasukContainer = createAppContainer(SuratMasukTab);

//export default SuratMasukScreen;

export default class SuratMasukDetail extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            data: {
                can: {
                    set_kehadiran: false,
                    disposisi_surat: false,
                    laporan_kegiatan: false
                },
                surat: {
                    
                },
                laporan_kegiatan: {

                },
                list_disposisi: [],
                tujuan_disposisi: []
            }
        }
        //console.log(this.state.data);
    }

    
    static navigationOptions = ({navigation}) => {
        return {
            title: 'Detail Surat',
            headerRight: (<TouchableOpacity 
                style={{marginRight: 20, width: 35, height: 35, alignContent: 'center', justifyContent: 'center'}}
                onPress={navigation.getParam('reload')}
            >
                <Ionicons name='md-refresh' size={25} color='white' />
            </TouchableOpacity>)
        }
    };


    reload = () => {
        console.log('reloading...');
        const id = this.props.navigation.getParam('id');
        db.transaction(
            tx => {
                tx.executeSql('SELECT token FROM akun', [], (_,{rows}) => {
                    if(rows.length >= 1){
                        Server.SuratMasukDetail(rows._array[0].token, id).then(
                            (data) => {
                                if(data.status == false){
                                    alert(data.message);
                                } else {
                                    console.log('done');

                                    this.setState({
                                        data: data.data
                                    })
                                }
                            }
                        );
                    }
 
                });    
            }
        );
    }
    componentDidMount() {
        this.reload();
        this.props.navigation.setParams({reload: this.reload});
    };
    
    render() {
        console.log('renew');
        return (
            <View style={{flex: 1}}>
                <SuratMasukContainer screenProps={{data: this.state.data, navigation: this.props.navigation, reload: () => this.reload()}}/>
            </View>
           
        )
    }
}

