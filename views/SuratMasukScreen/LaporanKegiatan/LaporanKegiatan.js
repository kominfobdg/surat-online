import React from 'react';
import {StyleSheet, View, Image, TextInput, Text, TouchableOpacity, ScrollView, ActivityIndicator, Alert} from 'react-native';
import { FileSystem, SQLite, ImagePicker, Permissions } from 'expo';
import Server from '../../Common/Server';
import Modal from 'react-native-modal';

const db = SQLite.openDatabase('akun.db');

export default class LaporanKegiatan extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            height: 50,
            photo: null,
            pembicara: '',
            judul: '',
            ringkasan: '',
            isUploading: false,
            isLoading: false,
            
        }
    }
    
    setPhoto(pt){
        this.setState({photo: pt, isLoading: true, isSelect: true});
        
    }

    kirimLaporan() {
        const id = this.props.navigation.getParam('id');

        if(this.state.photo == null){
            alert('Pilih foto terlebih dahulu');
            return;
        }
        this.setState({
            isUploading: true
        })
        db.transaction(
            tx => {
                tx.executeSql('SELECT token FROM akun', [], (_,{rows}) => {
                    if(rows.length >= 1){
                        Server.KirimLaporan(rows._array[0].token, id, this.state.pembicara, this.state.judul, this.state.ringkasan, this.state.photo).then(
                            (data) => {
                                this.setState({
                                    isUploading: false
                                })
                                if(data.status == false){
                                    alert(data.message);
                                    
                                } else {
                                    alert(data.message);
                                    this.props.navigation.goBack();
                                }
                            }
                        ).catch(err => {
                            this.setState({
                                isUploading: false
                            });
                            alert(err);
                        });
                    }
                });
            }
        )
    }

    async takePicture() {
        const {
            status: cameraPerm
          } = await Permissions.askAsync(Permissions.CAMERA);
      
          const {
            status: cameraRollPerm
          } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
          this.setState({
              isLoading: true
          })
          // only if user allows permission to camera AND camera roll
          if (cameraPerm === 'granted' && cameraRollPerm === 'granted') {
            let pickerResult = await ImagePicker.launchCameraAsync({
              
            });
            this.setState({
                photo: pickerResult,
                isLoading: false
            });
            //this.setPhoto(pickerResult);
          }
    }

    async pickImage(){
        const {
            status: cameraPerm
          } = await Permissions.askAsync(Permissions.CAMERA);
      
          const {
            status: cameraRollPerm
          } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
          this.setState({
              isLoading: true
          })
          // only if user allows permission to camera AND camera roll
          if (cameraPerm === 'granted' && cameraRollPerm === 'granted') {
            let pickerResult = await ImagePicker.launchImageLibraryAsync({
              
            });
            this.setState({
                photo: pickerResult,
                isLoading: false
            });
            //this.setPhoto(pickerResult);
          }
    }

    render() {
        const { data, navigation } = this.props;
        let msg = 'Tekan area ini untuk mengganti foto kegiatan';
        if(this.state.photo == null){
            msg = 'Belum ada foto kegiatan yang dilampirkan. Tekan area ini untuk mengambil foto kegiatan';
        }
        if(data.can.laporan_kegiatan == false){
            return (
                <View style={styles.container}>
                    <Text style={{fontSize: 16}} >Anda tidak bisa melaporkan kegiatan</Text>
                </View>
            )
        }
        return (
            <View style={styles.container}>
                <ScrollView style={{flex: 1, paddingHorizontal: 10, marginTop: 10}}>
                <Text style={{fontSize: 16}}>Silahkan lengkapi formulir di bawah ini untuk mengisi laporan kegiatan Surat Masuk yang telah berlangsung.</Text>
                <Text style={styles.title}>Pembicara</Text>
                <TextInput
                    style={styles.input}
                    placeholder='nama pembicara di dalam kegiatan'
                    placeholderTextColor='#808080'
                    onChangeText={(text) => this.setState({pembicara: text})}
                />
                <Text style={styles.title}>Judul</Text>
                <TextInput
                    style={styles.input}
                    placeholder='nama judul kegiatan'
                    placeholderTextColor='#808080'
                    onChangeText={(text) => this.setState({judul: text})}
                />
                <Text style={styles.title}>Ringkasan</Text>
                <TextInput
                    style={{
                        backgroundColor: '#80808030',
                        height: Math.max(200, this.state.height+20),
                        marginVertical: 5,
                        paddingHorizontal: 10,
                        textAlignVertical: 'top'
                    }}
                    placeholder='penjelasan singkat mengenai isi kegiatan yang telah berlangsung'
                    placeholderTextColor='#808080'
                    multiline={true}
                    onChangeText={(text) => this.setState({ringkasan: text})}
                    onContentSizeChange={(event) => {
                        this.setState({
                            height: event.nativeEvent.contentSize.height
                        })
                    }}
                />
                <Text style={styles.title}>Dokumentasi Foto</Text>
                <TouchableOpacity onPress={() => Alert.alert('Pilih metode', 'pilih metode pengambilan gambar', [{text: 'Ambil dengan kamera', onPress: () => this.takePicture()}, {text: 'Pilih dari galeri', onPress: () => this.pickImage()}])}>
                    <View style={styles.camera}>
                        {
                            (this.state.photo != null) ? (<Image source={{uri: this.state.photo.uri}} style={styles.photo} />)  : 
                            (<Image source={require('../../../assets/images/camera.png')} style={styles.ic_camera} />)
                        }
                        {
                            (this.state.photo == null && !this.state.isLoading) ? (<Text style={styles.title}>{msg}</Text>) :
                            (<View></View>)
                        }
                        {
                            (this.state.isLoading) ? <ActivityIndicator size="large" style={{alignSelf: 'center'}}/> : <Text></Text>
                        }
                        
                        
                    </View>
                </TouchableOpacity>
                
                <View style={{flexDirection: 'row'}}>
                
                </View>
                </ScrollView>
                <View style={styles.footer}>
                    <TouchableOpacity style={styles.button} onPress={() => this.kirimLaporan()}>
                        <View>
                            <Text style={styles.btn_text}>KIRIM LAPORAN KEGIATAN</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <Modal isVisible={this.state.isUploading} style={{marginVertical: 250, marginHorizontal: 50, backgroundColor: 'white', borderRadius: 5}}>
                    <View style={{ flex: 1, alignContent: 'center', justifyContent: 'center' }}>
                        <Text style={{fontSize: 16, color:'black', justifyContent: 'center', alignSelf: 'center'}}>Sedang Memproses</Text>
                        <ActivityIndicator
                            size='large'
                        />
                    </View>
                </Modal>
            </View>
        );
    }

   
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgb(242, 241, 242)',
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold',
        marginVertical: 5,
        flex: 1
    },
    input: {
        backgroundColor: '#80808030',
        height: 50,
        marginVertical: 5,
        paddingHorizontal: 10
    },
    button: {
        backgroundColor: 'rgb(254, 89, 62)',
        borderRadius: 25,
        height: 50,
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',

    },
    btn_text: {
        color: 'white',
        fontWeight: 'bold',
    },
    footer: {
        height: 65,
        backgroundColor: 'white',
        justifyContent: 'center'
    },
    camera: {
        backgroundColor: '#80808030',
        marginVertical: 5,
        paddingHorizontal: 10,
        marginBottom: 20,
        flexDirection: 'row',
    },
    ic_camera: {
        height: 50,
        width: 50,
        alignSelf: 'center',
        marginRight: 10,
    },
    photo: {
        height: 200,
        width: 200,
        resizeMode: 'contain',
    }
});