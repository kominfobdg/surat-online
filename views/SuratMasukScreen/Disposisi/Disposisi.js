import React from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, TextInput, Button, ActivityIndicator } from 'react-native';
import { Constants } from 'expo';
import { CheckBox } from 'react-native-elements';
import Server from '../../Common/Server';
import { SQLite } from 'expo';
import Modal from 'react-native-modal';

const db = SQLite.openDatabase('akun.db');


export default class Disposisi extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            height: 50,
            status: false,
            catatan: '',
            isLoading: false
        }
    }
    
    check(id){
        //alert(id);
        let d = this.state.data;
        d[id].checked = !d[id].checked;
        //console.log(this.state.check[0]);
        this.setState((state) => ({
            data: d,
            status: !state.status
        }))
    }

    KirimDisposisi(){
        const id = this.props.navigation.getParam('id');
        const reload = this.props.navigation.getParam('reload');
        let id_user = [];
        for(var i = 0; i<this.state.data.length; i++){
            if(this.state.data[i].checked){
                id_user.push(this.state.data[i].id);
            }
        }
        this.setState({isLoading: true});
        db.transaction(
            tx => {
                tx.executeSql('SELECT token FROM akun', [], (_, {rows}) => {
                    Server.KirimDisposisi(rows._array[0].token, id, id_user, this.state.catatan).then(
                        (responseData) => {
                            this.setState({isLoading: true});
                            if(responseData.status){
                                alert(responseData.message);
                                reload.reload();
                                this.props.navigation.goBack();
                            } else {
                                alert('Surat gagal didisposisi');
                            }
                        }
                    ).catch((err) => {
                        this.setState({isLoading: true});
                        alert(err);
                    });
                })
            }
        )
        
    }

    componentDidMount() {
        
        const data = this.props.navigation.getParam('data');
        
        for(var i = 0; i<data.length; i++){
            data[i].idx = i;
            data[i].checked = false;
        }
        this.setState({
            data: data,
        })
        
    }

    static navigationOptions = {
        title: 'Disposisi Surat'
    };

    
    render() {
        //const data = this.props.navigation.getParam('data');
        //const reload = this.props.navigation.getParam('reload');
        //console.log(reload);
        //reload();
        return (
            <View style={styles.container}>
                <Text style={styles.title}>Tujuan</Text>
                <FlatList
                    style={styles.list}
                    data={this.state.data}
                    extraData={this.state.status}
                    renderItem={({item}) => <CheckBox
                        title={item.nama}
                        checked={item.checked}
                        onPress={() => this.check(item.idx)}
                    />}
                    keyExtractor={(item, index) => index.toString()}
                />
                <Text style={styles.title}>Catatan</Text>
                <TextInput
                    style={{
                        backgroundColor: '#80808030',
                        height: Math.max(200, this.state.height+20),
                        margin: 10,
                        padding: 10,
                        textAlignVertical: 'top'
                    }}
                    onChangeText={(text) => this.setState({catatan: text})}
                    placeholder='catatan untuk disposisi'
                    placeholderTextColor='#808080'
                    multiline={true}
                    onContentSizeChange={(event) => {
                        this.setState({
                            height: event.nativeEvent.contentSize.height
                        })
                    }}
                />
                <View style={styles.footer}>
                    <TouchableOpacity style={styles.button} onPress={() => this.KirimDisposisi()}>
                        <View>
                            <Text style={styles.btn_text}>KIRIM</Text>
                        </View>
                    </TouchableOpacity>
                    
                </View>
                <Modal isVisible={this.state.isLoading} style={{marginVertical: 250, marginHorizontal: 50, backgroundColor: 'white', borderRadius: 5}}>
                    <View style={{ flex: 1, alignContent: 'center', justifyContent: 'center' }}>
                        <Text style={{fontSize: 16, color:'black', justifyContent: 'center', alignSelf: 'center'}}>Sedang Memproses</Text>
                        <ActivityIndicator
                            size='large'
                        />
                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgb(242, 241, 242)'
    },
    button: {
        backgroundColor: 'rgb(254, 89, 62)',
        borderRadius: 25,
        height: 50,
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',

    },
    btn_text: {
        color: 'white',
        fontWeight: 'bold',
    },
    footer: {
        height: 65,
        backgroundColor: 'white',
        justifyContent: 'center'
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold',
        margin: 10
    },
    list: {
        
    }
});