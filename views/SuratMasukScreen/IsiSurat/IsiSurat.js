import React, { Component } from 'react';
import {StyleSheet, View, Image, WebView, Text, TextInput, TouchableOpacity, ScrollView} from 'react-native';
import Helper from '../../Common/Helper';

export default class IsiSurat extends React.Component {
    render() {
        const {data, navigation} = this.props;
        
        return (
            <View style={{flex: 1}}>
                <View style={{flex: 1}}>
                    <View style={styles.top}>
                       
                    </View>
                    <View style={styles.center}>
                        
                    </View>
                    <View style={styles.content}>
                        <Text style={styles.perihal}>{data.surat.perihal}</Text>
                        <View style={styles.box}>
                            <View style={{flexDirection: 'row', marginBottom: 10}}>
                                <View style={styles.icon}>
                                    <Image source={{uri: data.surat.foto_pengirim}} style={styles.image} />
                                </View>
                                <View style={{flex: 1, marginLeft: 10}}>
                                    <Text style={styles.title}>{data.surat.pengirim}</Text>
                                    <Text style={styles.waktu}>{Helper.Date(data.surat.tanggal)}</Text>
                                </View>
                            </View>
                            <Text style={styles.title}>{data.surat.nomor_surat}</Text>
                            <Text style={{fontSize: 14}}>Waktu pelaksanaan kegiatan dari surat masuk yaitu {data.surat.waktu_acara}</Text>
                            <Text>Silahkan lihat detail selengkapnya surat ini dengan menekan tombol Lihat Surat yang berwarna orange</Text>
                        </View>
                        {(data.surat.lampiran == null) ? <View></View> :
                        <TouchableOpacity style={{marginTop: 20}} onPress={() => navigation.navigate('PDFViewer', {url: data.surat.lampiran})}>
                            <View style={styles.box}>
                                <Text>Terdapat lampiran pada surat ini, klik disini untuk melihat lampiran</Text>
                            </View>
                        </TouchableOpacity>
                        }
                    </View>
                </View>
                <View style={styles.footer}>
                    <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('PDFViewer', {url: data.surat.file_surat})}>
                        <View>
                            <Text style={styles.btn_text}>LIHAT SURAT</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            
        )
    }
}


const styles = StyleSheet.create({
    image: {
        height: 30,
        width: 30,
        alignSelf: 'center'
    },
    perihal: {
        color: 'white',
        marginVertical: 10,
        fontSize: 14,
        fontWeight: 'bold'
    },
    content: {
        position: 'absolute',
        minHeight: 300,
        width: '90%',
        alignSelf: 'center'
    },
    box: {
        flex: 1,
        backgroundColor: 'white',
        borderRadius: 10,
        padding: 10,
    },
    top: {
        flex: 1,
        backgroundColor: '#1976D2',
    },
    center: {
        flex: 1,
        backgroundColor: 'rgb(242, 241, 242)'
    },
    list: {
        flex: 1,
        borderWidth: 1,
        borderBottomColor: '#80808030',
        borderTopColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        margin: 10,
    },
    container: {
        flex: 1,
        flexDirection: 'row',
    },
    read: {
        width: 24,
        height: 24,
        paddingBottom: 10,
        marginLeft: 5,
    },
    icon: {
        borderRadius: 50,
        backgroundColor: '#80808030',
        width: 50,
        height: 50,
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 14,
        fontWeight: '500'
    },
    pengirim: {
        color: 'rgb(255,87,51)',
        fontSize: 16
    },
    tujuan: {
        fontSize: 16
    },
    waktu: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#0000005f',
    },
    button: {
        backgroundColor: 'rgb(254, 89, 62)',
        borderRadius: 25,
        height: 50,
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',

    },
    btn_text: {
        color: 'white',
        fontWeight: 'bold',
    },
    footer: {
        height: 65,
        backgroundColor: 'white',
        justifyContent: 'center'
    },
})