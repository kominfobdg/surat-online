import React, { Component } from 'react'
import {StyleSheet, View, Image, Button, Text, TextInput, TouchableOpacity, FlatList} from 'react-native'
import SuratMasukRow from './SuratMasukRow';


const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

const SuratMasukListView = ({ itemlList, homeNav }) => (
    <View style={styles.container}>
        <FlatList
            data={itemlList}
            renderItem={({ item }) => <SuratMasukRow
                sender={item.pengirim}
                title={item.perihal}
                content={item.isi_surat}
                date={item.tanggal}
                isRead={item.status_dibaca == null ? false : true}
                id={item.id}
                homeNav={homeNav}
            />}
            keyExtractor={(item, index) => index.toString()}
        />
    </View>
);

export default SuratMasukListView;



