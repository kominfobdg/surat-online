import React, { Component } from 'react'
import {StyleSheet, View, Image, Text, TouchableOpacity} from 'react-native'
import Helper from '../Common/Helper';

const SuratMasukRow = ({ sender, title, content, date, isRead, id, homeNav }) => (
    
    <View style={{flex: 1, marginHorizontal: 10}}>
        <TouchableOpacity style={{flex: 1}} onPress={() => homeNav.navigate('SuratMasuk', {id: id})}>
            <View style={styles.container}>
                
                {isRead == true ? <Image source={require('../../assets/images/ic_email_read.png')} style={styles.photo_read} /> : <Image source={require('../../assets/images/ic_email_unread.png')} style={styles.photo_unread} /> }
                
                
                <View style={styles.container_text} >
                    {isRead == true ? <Text style={styles.sender_read}>{sender}</Text> : <Text style={styles.sender}>{sender}</Text> }
                    {isRead == true ? <Text style={styles.title_read}>{title}</Text> : <Text style={styles.title}>{title}</Text> }
                    {isRead == true ? <Text style={styles.content_read}>{content}</Text> : <Text style={styles.content}>{content}</Text> }
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'flex-end'}}>
                        {isRead == true ? <Text style={styles.date_read}>{Helper.Date(date)}</Text> : <Text style={styles.date}>{Helper.Date(date)}</Text> }
                        {isRead == true ? <Image source={require('../../assets/images/ic_read.png')} style={styles.read} /> : <Image source={require('../../assets/images/ic_unread.png')} style={styles.read} />  }
                    </View>
                    
                </View>
            </View>
        </TouchableOpacity>
    </View>
    
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        padding: 10,
        marginVertical: 10,
        borderRadius: 2,
        backgroundColor: '#FFF',
        elevation: 2
    },
    sender: {
        fontSize: 16,
        color: '#000',
        fontWeight: 'bold'
    },
    sender_read: {
        fontSize: 16,
        color: '#0000005f',
        fontWeight: 'bold'
    },
    title: {
        fontSize: 15,
        color: '#000'
    },
    title_read: {
        fontSize: 15,
        color: '#0000005f'
    },
    date: {
        alignSelf: 'flex-end',
        fontSize: 12,
        fontWeight: 'bold'
    }, 
    date_read: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#0000005f',
    }, 
    container_text: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 12,
        justifyContent: 'center',
    },
    content: {
        fontSize: 14,
    },
    content_read: {
        fontSize: 14,
        color: '#0000005f',
    },
    photo_read: {
        height: 45,
        width: 45
    }, 
    photo_unread: {
        height: 38,
        width: 45
    },
    icon: {
        backgroundColor: '#EEEEEE',
        width: 55,
        height: 55,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    read: {
        width: 24,
        height: 24,
        paddingBottom: 10,
        marginLeft: 5,
    },
});

export default SuratMasukRow;