import React, { Component } from 'react'
import {StyleSheet, View, Image, Button, Text, TextInput, TouchableOpacity, FlatList} from 'react-native'
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { SQLite } from 'expo';
import SuratMasukListView from './SuratMasukListView';
import Server from '../Common/Server';

const db = SQLite.openDatabase('akun.db');

export default class SuratMasukList extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            data: [],
            search: null
        };
        if(this.props.badge != null){
            this.props.badge.setRefreshKeluar(() => this.refresh());
        }
        
        
    }

    refresh(){
        console.log('refreshing surat masuk');
        const search = this.props.search;
        db.transaction(
            tx => {
                tx.executeSql('SELECT token FROM akun', [], (_, {rows}) => {
                    if(rows.length >= 1){
                        let token = rows._array[0].token;
                        Server.SuratMasukList(token, search).then(
                            (responseData) => {
                                if(responseData.status == false){
                                    alert(responseData.message);
                                } else {
                                    var count = 0;
                                    for(var i = 0; i < responseData.data.length; i++){
                                        if(responseData.data[i].status_dibaca == null){
                                            count++;
                                        }
                                    }
                                    if(this.props.badge != null){
                                        this.props.badge.updateMasuk(count);
                                    }
                                    
                                    this.setState({
                                        data: responseData.data
                                    });
                                    
                                }
                            }
                        )
                        .catch((err) => alert(err));
                    }
                });
            }
        );
    }

    componentDidUpdate(){
        const search = this.props.search;
        if(search != this.state.search){
            this.setState({
                search: search
            })
            this.refresh();
        }
    }

    componentDidMount() {
        this.refresh();
    }

    render() {
        if(this.props.badge != null){
            return (
                <SuratMasukListView
                    itemlList={this.state.data}
                    homeNav={this.props.badge.homeNav}
                />
            )
        } else {
            return (
                <SuratMasukListView
                    itemlList={this.state.data}
                    homeNav={this.props.navigation}
                />
            )
        }
        
    }

    
}