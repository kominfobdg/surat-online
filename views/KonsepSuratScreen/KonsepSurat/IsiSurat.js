import React, { Component } from 'react';
import { WebView, View, Text } from 'react-native';
import { SQLite } from 'expo';
import Server from '../../Common/Server';

const db = SQLite.openDatabase('akun.db');


class IsiSurat extends Component {

    constructor(props){
        super(props);
        
        this.state = {
            token: null,
            id: this.props.navigation.getParam('id')
        }
        db.transaction(
            tx => {
                tx.executeSql("SELECT token FROM akun", [], (_,{rows}) => {
                    if(rows.length > 0){
                        let token = rows._array[0].token;
                        this.setState({
                            token: token
                        })
                    }
                })
            }
        )
    }

    componentDidUpdate(){
        
    }


    static navigationOptions = {
        title: 'Isi Surat'
    };

    render() {
        if(this.state.token == null && this.state.id == null){
            return (
                <View style={{flex: 1}}>
                    
                </View>
            )
        } else {
            return <WebView
                source={{uri: Server.KonsepSuratDetail(this.state.token, this.state.id)}}
            />
        }
        
    }

}


export default IsiSurat;