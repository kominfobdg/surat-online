import React from 'react';
import { View, StyleSheet, Text, FlatList, TouchableOpacity, TextInput } from 'react-native';
import CommonRow from '../../Common/CommonRow';

export default class Tembusan extends React.Component {
    static navigationOptions = {
        title: 'Tembusan'
    };
    render() {
        let tembusan = this.props.navigation.getParam('tembusan');
        const mengubah = this.props.navigation.getParam('mengubah');
        tembusan = tembusan == null ? [] : tembusan.split(';');
        return (
            <View style={styles.container}>
                <View style={{flexDirection: 'row'}}>
                    <TextInput
                        style={styles.input}
                        style={ mengubah ? styles.input : styles.input_off }
                        editable={mengubah}
                    />
                    <TouchableOpacity style={styles.button_small}>
                        <Text style={styles.btn_text}>+</Text>
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={tembusan}
                    renderItem={({ item }) => <CommonRow title={'Tembusan'} data={item} />}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    item: {
       
    },
    container: {
        flex: 1,
        paddingHorizontal: 10,
    }, 
    item: {
        fontSize: 16,
        padding: 5,
        borderWidth: 1,
        borderColor: 'black',
    },
    button_small: {
        backgroundColor: 'rgb(254, 89, 62)',
        borderRadius: 50,
        height: 40,
        width: 40,
        paddingHorizontal: 10,
        marginHorizontal: 5,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    btn_text: {
        color: 'white',
        fontWeight: 'bold',
    },
    input: {
        backgroundColor: '#80808030',
        height: 40,
        marginVertical: 5,
        paddingHorizontal: 10,
        flex: 1
    },
    input_off: {
        backgroundColor: '#808080f0',
        height: 40,
        marginVertical: 5,
        paddingHorizontal: 10,
        flex: 1
    },
})