import React from 'react';
import { StyleSheet, View, Text, ScrollView, TouchableOpacity, TextInput, Picker, PickerIOS, FlatList, Platform, DatePickerAndroid, DatePickerIOS } from 'react-native';
import Helper from '../../Common/Helper';
import { SQLite } from 'expo';
import Server from '../../Common/Server';
import { CheckBox } from 'react-native-elements';
import DatePicker from 'react-native-datepicker';
import ModalSelector from 'react-native-modal-selector';

const db = SQLite.openDatabase('akun.db');

const TujuanRow = ({nama}) => (
    <View style={{flex: 1, backgroundColor: 'white', marginVertical: 5}}>
        <TouchableOpacity>
            <Text style={{fontSize: 16, margin: 10}}>{nama}</Text>
        </TouchableOpacity>
    </View>
);

export default class KonsepSurat extends React.Component {
    constructor(props){
        super(props);
        const data = this.props.data;
        this.state = {
            data: data,
            kategori_surat: '',
            sifat_surat: '',
            updateList: false,
            master_data: {
                skpd_dari: [],
                skpd_tujuan: [],
                kategori_surat: [],
                sifat_surat: [],
            },
        }
    }
    componentDidMount() {
        
        db.transaction(
            tx => {
                tx.executeSql("SELECT token FROM akun", [], (_, {rows}) => {
                    if(rows.length > 0){
                        let token = rows._array[0].token;
                        Server.GetMasterData(token)
                        .then(
                            responseData => {
                                if(responseData.status){
                                    responseData.data.skpd_tujuan.map((value, i) => {
                                        responseData.data.skpd_tujuan[i].checked = false;
                                    });
                                    this.setState({
                                        master_data: responseData.data
                                    });
                                } else {
                                    alert(responseData.message);
                                }
                            }
                        )
                        .catch( err => alert(err));
                    }
                })
            }
        )
        const data = this.props.data;
        this.setState({
            data: data
        });
    }

    checkTujuan(id){
        let data = this.state.master_data;
        data.skpd_tujuan.map((d, i) => {
            if(d.id == id){
                data.skpd_tujuan[i].check = !data.skpd_tujuan[i].check;
            }
        });
        this.setState((state) => ({
            master_data: data,
            updateList: !state.updateList
        }))
    }

    componentDidUpdate(){
        
        const data = this.state.data;
        if(this.state.master_data.skpd_tujuan.length > 0 && data.surat.id_skpd_tujuan.length > 0 && !this.state.update_skpd_tujuan){
            let master = this.state.master_data;
            data.surat.id_skpd_tujuan.map((d, i) => {
                master.skpd_tujuan.map((da, j) => {
                    if(d == da.id){
                        master.skpd_tujuan[i].checked = true;
                    }
                });
            });
            this.setState({
                master_data: master,
                update_skpd_tujuan: true,
                kategori_surat: this.findLabel(this.state.master_data.kategori_surat, data.surat.id_kategori),
                sifat_surat: this.findLabel(this.state.master_data.sifat_surat, data.surat.id_sifat) 
            })
        }
        const sourceData = this.props.data;
        if(sourceData.surat.id != null && this.state.data.surat.id == null){
            this.setState({
                data: sourceData
            })
        }
    }

    findLabel(data, id) {
        let res = data[0].nama;
        data.map((value, idx) => {
            //console.log(id);
            if(value.id == id){
                console.log(value.nama);
                res = value.nama;
            }
        })
        if(data.length == 0) {
            console.log(data);
            return '';
        }
        //console.log(id);
        return res;
    }

    render(){
        const {navigation} = this.props;
        const kategori_surat = [];
        const sifat_surat = [];
        let data = this.state.data;
        let master_data = this.state.master_data;
        for(let i=0; i<master_data.kategori_surat.length; i++){
            let d = master_data.kategori_surat[i];
            kategori_surat.push({key: d.id, label: d.nama});
        }
        for(let i=0; i<master_data.sifat_surat.length; i++){
            let d = master_data.sifat_surat[i];
            sifat_surat.push({ label:d.nama, key:d.id});
        }
            
        return(
            <View style={styles.container}>
                <ScrollView style={styles.scroll}>
                    <Text style={styles.title}>Perangkat Daerah Pembuat</Text>
                    <TextInput
                        style={styles.input_off}
                        editable={false}
                        value=''
                    />
                    <Text style={styles.title}>Kategori Surat</Text>
                    <ModalSelector
                        data={kategori_surat}
                        initValue={this.state.kategori_surat}
                        style={ data.can.mengubah ? styles.input : styles.input_off }
                        accessible={data.can.mengubah}
                        //onChange={(option) => this.setState({kategori_surat: option.key})}
                    />
                    <Text style={styles.title}>Sifat Surat</Text>
                    <ModalSelector
                        data={sifat_surat}
                        initValue={this.state.sifat_surat}
                        style={ data.can.mengubah ? styles.input : styles.input_off }
                        accessible={data.can.mengubah}
                        //onChange={(option) => this.setState({kategori_surat: option.key})}
                    />
                    <Text style={styles.title}>Nomor Surat</Text>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <TextInput
                            style={styles.input_off}
                            editable={false}
                            value={data.surat.kode_kategori}
                        />
                        <Text style={styles.sparator}>/</Text>
                        <TextInput
                            style={ data.can.mengubah ? styles.input : styles.input_off }
                            editable={data.can.mengubah}
                            value={data.surat.nomor}
                        />
                        <Text style={styles.sparator}>/</Text>
                        <TextInput
                            style={styles.input_off}
                            editable={false}
                            value={data.surat.alias}
                        />
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 10}}>
                        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('TujuanPerangkatDaerah', {skpd_tujuan: master_data.skpd_tujuan, mengubah: data.can.mengubah})}>
                            <Text style={styles.btn_text}>Tujuan Perangkat Daerah</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 10}}>
                        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('TujuanLainya', {tujuan_lain: data.surat.tujuan_lain, mengubah: data.can.mengubah})}>
                            <Text style={styles.btn_text}>Tujuan Lainya</Text>
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.title}>Di</Text>
                    <TextInput
                        style={ data.can.mengubah ? styles.input : styles.input_off }
                        editable={data.can.mengubah}
                        value={data.surat.tempat_tujuan}
                        onChangeText={(text) => {
                            data.surat.tempat_tujuan = text;
                            this.setState({data: data})
                        }}
                    />
                    <Text style={styles.title}>Perihal</Text>
                    <View style={{flexDirection: 'row'}}>
                        <TextInput
                            style={ data.can.mengubah ? styles.input : styles.input_off }
                            editable={data.can.mengubah}
                            value={data.surat.perihal}
                        />
                        <DatePicker
                            style={{flex: 0.8, marginLeft: 10, alignSelf: 'center' }}
                            date={data.surat.tanggal_surat}
                            mode="date"
                            placeholder="pilih tanggal"
                            format="YYYY-MM-DD"
                            confirmBtnText="OK"
                            cancelBtnText="Batal"
                            showIcon={false}
                            disabled={!data.can.mengubah}
                            onDateChange={(date) => {
                                data.surat.tanggal_surat = date;
                                this.setState({
                                    data: data
                                })
                            }}
                            customStyles={{
                                dateInput: data.can.mengubah ? styles.input : styles.input_off
                            }}
                        />
                    </View>
                    
                    <Text style={styles.title}>Lokasi</Text>
                    <TextInput
                        style={ data.can.mengubah ? styles.input : styles.input_off }
                        editable={data.can.mengubah}
                        value={data.surat.lokasi_acara}
                    />
                    <Text style={styles.title}>Tanggal</Text>
                    <View style={{flexDirection: 'row'}}>
                        <DatePicker
                            style={{flex: 1, alignSelf: 'center'}}
                            date={data.surat.tanggal_mulai_acara}
                            mode="date"
                            placeholder="pilih tanggal"
                            format="YYYY-MM-DD"
                            confirmBtnText="OK"
                            cancelBtnText="Batal"
                            showIcon={false}
                            disabled={!data.can.mengubah}
                            onDateChange={(date) => {
                                data.surat.tanggal_mulai_acara = date;
                                this.setState({
                                    data: data
                                })
                            }}
                            customStyles={{
                                dateInput: data.can.mengubah ? styles.input : styles.input_off
                            }}
                        />
                        <Text style={{marginHorizontal: 5, alignSelf: 'center'}}>sampai</Text>
                        <DatePicker
                            style={{flex: 1, alignSelf: 'center'}}
                            date={data.surat.tanggal_selesai_acara}
                            mode="date"
                            placeholder="pilih tanggal"
                            format="YYYY-MM-DD"
                            confirmBtnText="OK"
                            cancelBtnText="Batal"
                            showIcon={false}
                            disabled={!data.can.mengubah}
                            onDateChange={(date) => {
                                data.surat.tanggal_selesai_acara = date;
                                this.setState({
                                    data: data
                                })
                            }}
                            customStyles={{
                                dateInput: data.can.mengubah ? styles.input : styles.input_off
                            }}
                        />
                    </View>
                    <Text style={styles.title}>Pukul</Text>
                    <View style={{flexDirection: 'row'}}>
                        <DatePicker
                            style={{flex: 1, alignSelf: 'center'}}
                            date={data.surat.jam_mulai_acara}
                            mode="time"
                            placeholder="pilih tanggal"
                            format="HH:mm"
                            confirmBtnText="OK"
                            cancelBtnText="Batal"
                            showIcon={false}
                            disabled={!data.can.mengubah}
                            is24Hour={true}
                            onDateChange={(date) => {
                                data.surat.jam_mulai_acara = date;
                                this.setState({
                                    data: data
                                })
                            }}
                            customStyles={{
                                dateInput: data.can.mengubah ? styles.input : styles.input_off
                            }}
                        />
                        <Text style={{marginHorizontal: 5, alignSelf: 'center'}}>sampai</Text>
                        <DatePicker
                            style={{flex: 1, alignSelf: 'center'}}
                            date={data.surat.jam_selesai_acara}
                            mode="time"
                            placeholder="pilih tanggal"
                            format="HH:mm"
                            confirmBtnText="OK"
                            cancelBtnText="Batal"
                            showIcon={false}
                            disabled={!data.can.mengubah}
                            is24Hour={true}
                            onDateChange={(date) => {
                                data.surat.jam_selesai_acara = date;
                                this.setState({
                                    data: data
                                })
                            }}
                            customStyles={{
                                dateInput: data.can.mengubah ? styles.input : styles.input_off
                            }}
                        />
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 10}}>
                        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Tembusan', {tembusan: data.surat.tembusan, mengubah: data.can.mengubah})}>
                            <Text style={styles.btn_text}>Tembusan</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 10}}>
                        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('IsiSuratKonsep', {isi_surat: data.surat.isi_surat, mengubah: data.can.mengubah})}>
                            <Text style={styles.btn_text}>Isi Surat</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <View style={styles.footer}>
                    <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('PDFViewer', {url: data.surat.file_surat})}>
                        <View>
                            <Text style={styles.btn_text}>PERTINJAU</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => alert('test')}>
                        <View>
                            <Text style={styles.btn_text}>UBAH</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    scroll: {
        flex: 1,
        paddingHorizontal: 10
    },
    container: {
        flex: 1,
        backgroundColor: 'rgb(242, 241, 242)',
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold',
        marginVertical: 5,
        flex: 1
    },
    input: {
        backgroundColor: '#80808030',
        height: 40,
        marginVertical: 5,
        paddingHorizontal: 10,
        flex: 1
    },
    input_off: {
        backgroundColor: '#808080a0',
        height: 40,
        marginVertical: 5,
        paddingHorizontal: 10,
        flex: 1
    },
    sparator: {
        paddingHorizontal: 5,
        fontSize: 30
    },
    button: {
        backgroundColor: 'rgb(254, 89, 62)',
        borderRadius: 25,
        height: 50,
        marginHorizontal: 0,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },

    button_small: {
        backgroundColor: 'rgb(254, 89, 62)',
        borderRadius: 50,
        height: 40,
        width: 40,
        paddingHorizontal: 10,
        marginHorizontal: 5,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    btn_text: {
        color: 'white',
        fontWeight: 'bold',
    },
    footer: {
        height: 65,
        backgroundColor: 'white',
        justifyContent: 'center',
        flexDirection: 'row',
        paddingTop: 10
    },
    list: {
        height: 200
    }
})