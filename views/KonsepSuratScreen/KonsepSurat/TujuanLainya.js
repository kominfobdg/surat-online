import React from 'react';
import { View, StyleSheet, Text, FlatList, TouchableOpacity, TextInput } from 'react-native';
import CommonRow from '../../Common/CommonRow';

export default class TujuanLainya extends React.Component {
    static navigationOptions = {
        title: 'Tujuan Lainya'
    };
    render() {
        let tujuan_lain = this.props.navigation.getParam('tujuan_lain');
        const mengubah = this.props.navigation.getParam('mengubah');
        tujuan_lain = tujuan_lain == null ? [] : tujuan_lain.split(';');
        return (
            <View style={styles.container}>
                <View style={{flexDirection: 'row'}}>
                    <TextInput
                        style={styles.input}
                        style={ mengubah ? styles.input : styles.input_off }
                        editable={mengubah}
                    />
                    <TouchableOpacity style={styles.button_small}>
                        <Text style={styles.btn_text}>+</Text>
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={tujuan_lain}
                    renderItem={({ item }) => <CommonRow title={'Tujuan'} data={item} />}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 10
    }, 
    item: {
        fontSize: 16,
        marginVertical: 5
    },
    button_small: {
        backgroundColor: 'rgb(254, 89, 62)',
        borderRadius: 50,
        height: 40,
        width: 40,
        paddingHorizontal: 10,
        marginHorizontal: 5,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    btn_text: {
        color: 'white',
        fontWeight: 'bold',
    },
    input: {
        backgroundColor: '#80808030',
        height: 40,
        marginVertical: 5,
        paddingHorizontal: 10,
        flex: 1
    },
    input_off: {
        backgroundColor: '#808080f0',
        height: 40,
        marginVertical: 5,
        paddingHorizontal: 10,
        flex: 1
    },
})