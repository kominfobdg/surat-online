import React from 'react';
import { View, StyleSheet, Text, FlatList } from 'react-native';
import { CheckBox } from 'react-native-elements';

export default class TujuanPerangkatDaerah extends React.Component {
    static navigationOptions = {
        title: 'Tujuan Perangkat Daerah'
    };
    render(){
        const skpd_tujuan = this.props.navigation.getParam('skpd_tujuan');
        const mengubah = this.props.navigation.getParam('mengubah');
        return (
            <View style={styles.container}>
                <FlatList
                    data={skpd_tujuan}
                    renderItem={({ item }) => <CheckBox
                        title={item.nama}
                        checked={item.checked}
                    /> }
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})