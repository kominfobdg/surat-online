import React from 'react';
import {View, StyleSheet, Text, TextInput, TouchableOpacity, ActivityIndicator} from 'react-native';
import { SQLite } from 'expo'
import Server from '../../Common/Server';
import Modal from 'react-native-modal';


const db = SQLite.openDatabase('akun.db');
export default class Validasi extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            pin: '',
            isLoading: false
        }

        
    }

    static navigationOptions = {
        title: 'Validasi'
    }

    validasi(){
        var id = this.props.navigation.getParam('id');
        var siap_kirim = this.props.navigation.getParam('siap_kirim');
        this.setState({isLoading: true});
        db.transaction(
            tx => {
                tx.executeSql("SELECT * FROM akun", [], (_,{rows}) => {
                    if(rows.length > 0){
                        let token = rows._array[0].token;
                        Server.Validasi(token, id, siap_kirim, this.state.pin).then(
                            data => {
                                this.setState({isLoading: false});
                                if(data.status){
                                    alert(data.message);
                                    this.props.navigation.goBack();
                                } else {
                                    alert(data.message);
                                }
                            }
                        ).catch(err => {this.setState({isLoading: false});alert(err)});
                    }
                })
            }
        )
        
    }

    render(){
        return (
            <View style={{flex: 1}}>
                <View style={{flex: 1}}>
                    <View style={styles.top}>
                       
                    </View>
                    <View style={styles.center}>
                        
                    </View>
                    <View style={styles.content}>
                        <View style={styles.box}>
                            <Text style={styles.title}>Pin Tanda Digital</Text>
                            <TextInput style={styles.input}
                                placeholder='Masukkan pin tanda digital'
                       
                                onChangeText={(text) => this.setState({pin: text})}
                                autoCapitalize='none'
                                secureTextEntry={true}
                            />
                        </View>
                    </View>
                </View>
                <View style={styles.footer}>
                    <TouchableOpacity style={styles.button}  onPress={() => this.validasi() }>
                        <View>
                            <Text style={styles.btn_text}>VALIDASI</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <Modal isVisible={this.state.isLoading} style={{marginVertical: 250, marginHorizontal: 50, backgroundColor: 'white', borderRadius: 5}}>
                    <View style={{ flex: 1, alignContent: 'center', justifyContent: 'center' }}>
                        <Text style={{fontSize: 16, color:'black', justifyContent: 'center', alignSelf: 'center'}}>Sedang Memproses</Text>
                        <ActivityIndicator
                            size='large'
                        />
                    </View>
                </Modal>
            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    image: {
        height: 30,
        width: 30,
        alignSelf: 'center'
    },
    perihal: {
        color: 'white',
        marginVertical: 10,
        fontSize: 14,
        fontWeight: 'bold'
    },
    content: {
        position: 'absolute',
        minHeight: 300,
        width: '90%',
        alignSelf: 'center',
        marginTop: 30
    },
    box: {
        flex: 1,
        backgroundColor: 'white',
        borderRadius: 10,
        padding: 10,
    },
    top: {
        flex: 1,
        backgroundColor: '#1976D2',
    },
    center: {
        flex: 1,
        backgroundColor: 'rgb(242, 241, 242)'
    },
    list: {
        flex: 1,
        borderWidth: 1,
        borderBottomColor: '#80808030',
        borderTopColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        margin: 10,
    },
    container: {
        flex: 1,
        flexDirection: 'row',
    },
    read: {
        width: 24,
        height: 24,
        paddingBottom: 10,
        marginLeft: 5,
    },
    icon: {
        borderRadius: 50,
        backgroundColor: '#80808030',
        width: 50,
        height: 50,
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 14,
        fontWeight: '500'
    },
    pengirim: {
        color: 'rgb(255,87,51)',
        fontSize: 16
    },
    input: {
        backgroundColor: '#80808030',
        height: 50,
        marginVertical: 5,
        paddingHorizontal: 10
    },
    tt: {
        fontSize: 14,
        marginBottom: 10
    },
    waktu: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#0000005f',
    },
    button: {
        backgroundColor: 'rgb(254, 89, 62)',
        borderRadius: 25,
        height: 50,
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',

    },
    btn_text: {
        color: 'white',
        fontWeight: 'bold',
    },
    footer: {
        height: 65,
        backgroundColor: 'white',
        justifyContent: 'center'
    },
})