import React from 'react';
import {StyleSheet, View, Image, Text, TouchableOpacity, ActivityIndicator} from 'react-native';
import Helper from '../../Common/Helper';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import Server from '../../Common/Server';
import {SQLite} from 'expo';
import Modal from 'react-native-modal';

const db = SQLite.openDatabase('akun.db');

export default class KonsepSurat extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            selected: 1,
            isLoading: false
        }
    }

    kirimSurat() {
        this.setState({isLoading: true});
        const reload = this.props.navigation.getParam('reload');
        db.transaction(
            tx => {
                tx.executeSql("SELECT token FROM akun", [], (_,{rows}) => {
                    if(rows.length > 0){
                        let token = rows._array[0].token;
                        let id = this.props.data.surat.id;
                        Server.KonsepKirim(token, id).then(
                            data => {
                                this.setState({isLoading: false});
                                alert(data.message);
                                if(data.status){
                                    reload.reload();
                                    this.props.navigation.goBack();
                                }
                            }
                        ).catch( err => {
                            this.setState({isLoading: false});
                            alert(err);
                        })
                    }
                })
            }
        )
        
    }
    
    render(){
        const {data, navigation} = this.props;
        return (
            <View style={{flex: 1}}>
                <View style={{flex: 1}}>
                    <View style={styles.top}>
                       
                    </View>
                    <View style={styles.center}>
                        
                    </View>
                    <View style={styles.content}>
                        <Text style={styles.perihal}>{data.surat.perihal}</Text>
                        <View style={styles.box}>
                            <View style={{flexDirection: 'row', marginBottom: 10}}>
                                <View style={styles.icon}>
                                    <Image source={require('../../../assets/images/logo-bandung.png')} style={styles.image} />
                                </View>
                                <View style={{flex: 1, marginLeft: 10}}>
                                    <Text style={styles.title}>{data.surat.dibuat_oleh}</Text>
                                    <Text style={styles.waktu}>{Helper.Date(data.surat.dibuat_pada)}</Text>
                                </View>
                            </View>
                            <Text style={styles.title}>Diubah Terakhir oleh</Text>
                            <Text style={styles.biasa}>{data.surat.ubah_terakhir_oleh}</Text>
                            <Text style={styles.title}>Diubah Terakhir Pada</Text>
                            <Text style={styles.biasa}>{Helper.Date(data.surat.ubah_terakhir_pada)}</Text>

                            
                            
                        </View>
                    </View>
                </View>
                {(data.can.memvalidasi) ?
                <View style={styles.footer}>
                    <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('IsiKonsepSurat', {id: data.surat.id})}>
                        <View>
                            <Text style={styles.btn_text}>UBAH KONSEP</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button_yes} onPress={() => navigation.navigate('FormValidasi', {id: data.surat.id, siap_kirim: 1})}>
                        <View>
                            <Text style={styles.btn_text}>VALIDASI KIRIM</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                : <View></View>}
                {(data.can.mengirim && data.surat.siap_kirim == 1) ? 
                <View style={styles.footer}>
                    <TouchableOpacity style={styles.button} onPress={() => this.kirimSurat()}>
                        <View>
                            <Text style={styles.btn_text}>KIRIM SURAT</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                : <View></View>}
                <Modal isVisible={this.state.isLoading} style={{marginVertical: 250, marginHorizontal: 50, backgroundColor: 'white', borderRadius: 5}}>
                    <View style={{ flex: 1, alignContent: 'center', justifyContent: 'center' }}>
                        <Text style={{fontSize: 16, color:'black', justifyContent: 'center', alignSelf: 'center'}}>Sedang Memproses</Text>
                        <ActivityIndicator
                            size='large'
                        />
                    </View>
                </Modal>
            </View>
            
        )
        
        
    }
}
const styles = StyleSheet.create({
    image: {
        height: 30,
        width: 30,
        alignSelf: 'center'
    },
    perihal: {
        color: 'white',
        marginVertical: 10,
        fontSize: 14,
        fontWeight: 'bold'
    },
    content: {
        position: 'absolute',
        minHeight: 300,
        width: '90%',
        alignSelf: 'center'
    },
    box: {
        flex: 1,
        backgroundColor: 'white',
        borderRadius: 10,
        padding: 10,
    },
    top: {
        flex: 1,
        backgroundColor: '#1976D2',
    },
    center: {
        flex: 1,
        backgroundColor: 'rgb(242, 241, 242)'
    },
    list: {
        flex: 1,
        borderWidth: 1,
        borderBottomColor: '#80808030',
        borderTopColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        margin: 10,
    },
    container: {
        flex: 1,
        flexDirection: 'row',
    },
    read: {
        width: 24,
        height: 24,
        paddingBottom: 10,
        marginLeft: 5,
    },
    icon: {
        borderRadius: 50,
        backgroundColor: '#80808030',
        width: 50,
        height: 50,
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 14,
        fontWeight: '500'
    },
    pengirim: {
        color: 'rgb(255,87,51)',
        fontSize: 16
    },
    biasa: {
        fontSize: 14
    },
    waktu: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#0000005f',
    },
    button: {
        backgroundColor: 'rgb(254, 89, 62)',
        borderRadius: 25,
        height: 50,
        marginHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10,
        flex: 1
    },
    button_yes: {
        backgroundColor: 'green',
        borderRadius: 25,
        height: 50,
        marginHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10,
        flex: 1
    },
    btn_text: {
        color: 'white',
        fontWeight: 'bold',
    },
    footer: {
        height: 65,
        backgroundColor: 'white',
        justifyContent: 'center',
        flexDirection:'row'
    },
})