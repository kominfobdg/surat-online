import React, { Component } from 'react'
import {StyleSheet, View, Image, Button, Text, TextInput, TouchableOpacity, FlatList} from 'react-native'
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { SQLite } from 'expo';
import KonsepSuratList from './KonsepSuratList';
import KonsepSuratDetail from './KonsepSuratDetail';


const KonsepSuratNavigator = createStackNavigator({
    First: {
        screen: ({screenProps, navigation}) => <KonsepSuratList badge={screenProps} nav={navigation} />
    },
    KonsepSurat: KonsepSuratDetail
},
{
    headerMode: 'none',
    navigationOptions: {
        headerVisible: false
    }
});

const KonsepSuratNav = createAppContainer(KonsepSuratNavigator);

export default class KonsepSurat extends React.Component{
    constructor(props){
        super(props);
        //console.log(props.badge.screenProps);
        const { screenProps } = this.props.badge;
        //console.log(screenProps);
    }
    render() {
        const { screenProps } = this.props.badge;
        return (<KonsepSuratNav ref='konsepdetail' screenProps={{badge: screenProps}} />)
    }
}












