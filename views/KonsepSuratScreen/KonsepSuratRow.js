import React, { Component } from 'react'
import {StyleSheet, View, Image, Text, TouchableOpacity} from 'react-native'
import Helper from '../Common/Helper';

const KonsepSuratRow = ({ sender, title, content, date, siap_kirim, nomor_surat, perihal, kategori_surat, id, homeNav}) => (
    
    <View style={{flex: 1, marginHorizontal: 10}}>
        <TouchableOpacity style={{flex: 1}} onPress={() => homeNav.navigate('KonsepSurat', {id: id})}>
            <View style={styles.container}>
                {siap_kirim == '1' ? <Image source={require('../../assets/images/ic_suratkonsep_siapkirim.png')} style={styles.photo_unread} /> : <Image source={require('../../assets/images/ic_suratkonsep_belum_siapkirim.png')} style={styles.photo_unread} />}
                
                <View style={styles.container_text} >
                    <Text style={styles.sender}>{sender}</Text>
                    <Text style={styles.title}>{title}</Text>
                    <Text style={styles.perihal}>{perihal.toUpperCase() + ' (' + kategori_surat + ' - '+ nomor_surat.toUpperCase() + ')'}</Text>
                    <Text style={styles.content}>{content}</Text>
                    <View style={{flex: 1, flexDirection: 'row', alignSelf:'flex-end'}}>
                        <Text style={styles.date_read}>{Helper.Date(date)}</Text>
                        {siap_kirim == '1' ? <View style={styles.badge}><Text style={{color: 'white', fontSize: 12}}>SIAP DIKIRIM</Text></View> :
                                        <View></View> }
                    </View>
                    
                </View>
            </View>
        </TouchableOpacity>
    </View>
    
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        padding: 10,
        marginVertical: 10,
        borderRadius: 2,
        backgroundColor: '#FFF',
        elevation: 2
    },
    badge: {
        backgroundColor: 'rgb(23, 185, 120)',
        borderRadius: 10,
        width: 100,
        height: 18,
        alignItems: 'center',
        marginLeft: 5
    }, 
    badge_unread: {
        backgroundColor: '#808080',
        borderRadius: 10,
        width: 40,
        height: 18,
        alignItems: 'center',
        marginLeft: 5
    },  
    sender: {
        fontSize: 16,
        color: '#000',
        fontWeight: 'bold'
    },
    sender_read: {
        fontSize: 16,
        color: '#0000005f',
        fontWeight: 'bold'
    },
    title: {
        fontSize: 15,
        color: '#000'
    },
    title_read: {
        fontSize: 15,
        color: '#0000005f'
    },
    perihal: {
        fontSize: 15,
        color: '#7483b0'
    },
    perihal_read: {
        fontSize: 15,
        color: '#7483955f'
    },
    date: {
        alignSelf: 'flex-end',
        fontSize: 12,
        fontWeight: 'bold'
    }, 
    date_read: {
        alignSelf: 'flex-end',
        fontSize: 12,
        fontWeight: 'bold',
        color: '#0000005f',
    }, 
    container_text: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 12,
        justifyContent: 'center',
    },
    content: {
        fontSize: 14,
    },
    content_read: {
        fontSize: 14,
        color: '#0000005f',
    },
    photo_read: {
        height: 45,
        width: 45
    }, 
    photo_unread: {
        height: 45,
        width: 45
    },
    icon: {
        backgroundColor: '#EEEEEE',
        width: 55,
        height: 55,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default KonsepSuratRow;