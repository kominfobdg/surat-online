import React, { Component } from 'react'
import {StyleSheet, View, Image, Button, Text, TextInput, TouchableOpacity, FlatList} from 'react-native'
import KonsepSuratRow from './KonsepSuratRow';


const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

const KonsepSuratListView = ({ itemlList, homeNav }) => (
    <View style={styles.container}>
        <FlatList
            data={itemlList}
            renderItem={({ item }) => <KonsepSuratRow
                sender={item.pembuat}
                title={item.nama_skpd}
                content={item.isi_surat}
                nomor_surat={item.nomor_surat}
                perihal={item.perihal}
                date={item.tanggal_pembuatan}
                siap_kirim={item.siap_kirim}
                kategori_surat={item.kategori_surat}
                id={item.id}
                homeNav={homeNav}
            />}
            keyExtractor={(item, index) => index.toString()}
        />
    </View>
);

export default KonsepSuratListView;



