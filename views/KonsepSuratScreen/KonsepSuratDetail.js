import React from 'react';
import { StyleSheet, View, Text, TextInput } from 'react-native';
import { createMaterialTopTabNavigator, createAppContainer } from 'react-navigation';
import { SQLite } from 'expo';
import Server from '../Common/Server';
import KonsepSurat from './KonsepSurat/KonsepSurat';
import TeruskanKonsepList from './TeruskanKonsepSurat/TeruskanKonsepList';

const db = SQLite.openDatabase('akun.db');

const KonsepSuratTab = createMaterialTopTabNavigator(
    {
        KonsepSurat: {
            screen: (props) => <KonsepSurat data={props.screenProps.data} navigation={props.screenProps.navigation} reload={props.screenProps}/>,
            navigationOptions: {
                title: 'Konsep Surat'
            }
        },
        TeruskanKonsepList: {
            screen: (props) => <TeruskanKonsepList data={props.screenProps.data} navigation={props.screenProps.navigation} reload={props.screenProps} />,
            navigationOptions: {
                title: 'Teruskan Konsep Surat'
            },
        },
        
    },
    {
        tabBarOptions: {
            labelStyle: {
                fontSize: 14,
                fontWeight: 'bold'
            },
            style: {
                backgroundColor: '#1976D2'
            },
            upperCaseLabel: false,
            scrollEnabled: true
        }
    }
);

const KonsepSuratContainer = createAppContainer(KonsepSuratTab);

//export default SuratMasukScreen;

export default class KonsepSuratDetail extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            data: {
                can: {
                    mendisposisi: false,
                    mengubah: false,
                    memvalidasi: false,
                    mengirim: false
                },
                surat: {
                    id: null,
                },
                list_disposisi: [],
                tujuan_disposisi: []
            }
        }
        //console.log(this.state.data);
    }

    reload = () => {
        console.log('reloading konsep surat...');
        const id = this.props.navigation.getParam('id');
        db.transaction(
            tx => {
                tx.executeSql('SELECT token FROM akun', [], (_,{rows}) => {
                    if(rows.length >= 1){
                        Server.KonsepSuratDetail2(rows._array[0].token, id).then(
                            (data) => {
                                if(data.status == false){
                                    alert(data.message);
                                } else {
                                    console.log('done');

                                    this.setState({
                                        data: data.data
                                    })
                                }
                            }
                        );
                    }
 
                });    
            }
        );
    }

    
    static navigationOptions = {
        title: 'Detail Konsep Surat'
    };

    componentDidMount() {
        this.reload();
    };
    
    render() {
        
        return (
            <View style={{flex: 1}}>
                <KonsepSuratContainer screenProps={{data: this.state.data, navigation: this.props.navigation, reload: () => this.reload()}}/>
            </View>
           
        )
    }
}

