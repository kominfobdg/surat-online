import React, { Component } from 'react'
import {StyleSheet, View, Image, Button, Text, TextInput, TouchableOpacity, FlatList} from 'react-native'
import { SQLite } from 'expo';
import KonsepSuratListView from './KonsepSuratListView';
import Server from '../Common/Server';


const db = SQLite.openDatabase('akun.db');

export default class KonsepSuratList extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            data: [],
            search: null
        };
        if(this.props.badge != null){
            this.props.badge.setRefreshKonsep(() => this.refresh());
        }
        
    }

    refresh() {
        console.log('refreshing konsep surat');
        const search = this.props.search;
        db.transaction(
            tx => {
                tx.executeSql('SELECT token FROM akun', [], (_, {rows}) => {
                    if(rows.length >= 1){
                        let token = rows._array[0].token;
                        Server.KonsepSuratList(token, search)
                        .then((responseData) => {
                            if(responseData.status == false){
                                alert(responseData.message);
                            } else {
                                var count = 0;
                                for(var i = 0; i < responseData.data.length; i++){
                                    if(responseData.data[i].status_dibaca == null){
                                        count++;
                                    }
                                }
                                if(this.props.badge != null){
                                    this.props.badge.updateKonsep(count);
                                }
                                this.setState({
                                    data: responseData.data
                                });
                                
                            }
                        })
                        .catch((err) => alert(err));
                    }
                });
            }
        );
    }

    componentDidUpdate(){
        const search = this.props.search;
        if(search != this.state.search){
            this.setState({
                search: search
            })
            this.refresh();
        }
    }

    componentDidMount() {
        this.refresh();
    }


    render() {
        if(this.props.badge != null){
            return (
                <KonsepSuratListView
                    itemlList={this.state.data}
                    homeNav={this.props.badge.homeNav}
                />
            )
        } else {
            return (
                <KonsepSuratListView
                    itemlList={this.state.data}
                    homeNav={this.props.navigation}
                />
            )
        }
    }

   
}










