import React from 'react';
import {StyleSheet, View, Image, WebView, Text, TextInput, TouchableOpacity, ScrollView, ActivityIndicator} from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import Server from '../../Common/Server';
import {SQLite} from 'expo';
import Modal from 'react-native-modal';

const db = SQLite.openDatabase('akun.db');

export default class TeruskanKonsepSurat extends React.Component {

    constructor(props){
        super(props);
        
        this.state = {
            tujuan: [],
            selected: null,
            catatan: '',
            isLoading: false
        }
    }

    componentDidMount() {
        
        
       
    }

    kirim(){
        const id = this.props.navigation.getParam('id');
        const reload = this.props.navigation.getParam('reload');
        this.setState({isLoading: true});
        db.transaction(
            tx => {
                tx.executeSql('SELECT token FROM akun', [], (_,{rows}) => {
                    if(rows.length >= 1){
                        let token = rows._array[0].token
                        Server.DisposisiKonsepSurat(token, id, this.state.selected, this.state.catatan).then(
                            (data) => {
                                this.setState({isLoading: false});
                                alert(data.message);
                                reload.reload();
                                this.props.navigation.goBack();
                            }
                        )
                        .catch(err => {this.setState({isLoading: false});alert(err)});
                    } else {

                    }
                });
            }
        )
        
    }


    render(){
        const data = this.props.navigation.getParam('data');;
        let tujuan = []
        data.map((val, i) => {
            tujuan.push({key: i, label: val.nama, id: val.id});
        });
        return(
            <View style={styles.container}>
                <View style={{flex: 1}}>
                
                    <Text style={styles.title}>Tujuan</Text>
                    <ModalSelector
                        data={tujuan}
                        //style={styles.input}
                        initValue="--Pilih--"
                        onChange={(option) => this.setState({selected: option.id})}
                    />
                    <Text style={styles.title}>Catatan</Text>
                    <TextInput
                        style={{
                            backgroundColor: '#80808030',
                            height: '100%',
                            marginVertical: 5,
                            paddingHorizontal: 10,
                            textAlignVertical: 'top'
                        }}
                        placeholder='catatan mengenai surat yang akan dikirim'
                        placeholderTextColor='#808080'
                        multiline={true}
                        onChangeText={(text) => this.setState({catatan: text})}
                    />
                </View>
                <View style={styles.footer}>
                    <TouchableOpacity style={styles.button}  onPress={() => this.kirim() }>
                        <View>
                            <Text style={styles.btn_text}>KIRIM</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <Modal isVisible={this.state.isLoading} style={{marginVertical: 250, marginHorizontal: 50, backgroundColor: 'white', borderRadius: 5}}>
                    <View style={{ flex: 1, alignContent: 'center', justifyContent: 'center' }}>
                        <Text style={{fontSize: 16, color:'black', justifyContent: 'center', alignSelf: 'center'}}>Sedang Memproses</Text>
                        <ActivityIndicator
                            size='large'
                        />
                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 10,
        paddingTop: 10,
    },
    title: {
        fontSize: 14,
        fontWeight: '500'
    },
    input: {
        backgroundColor: '#80808030',
        height: 50,
        marginVertical: 5,
        paddingHorizontal: 10
    },
    button: {
        backgroundColor: 'rgb(254, 89, 62)',
        borderRadius: 25,
        height: 50,
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',

    },
    btn_text: {
        color: 'white',
        fontWeight: 'bold',
    },
    footer: {
        height: 65,
        
        backgroundColor: 'white',
        justifyContent: 'center'
    },
})