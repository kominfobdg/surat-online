import React from 'react';
import {StyleSheet, View, Image, Text, TouchableOpacity, FlatList, Alert} from 'react-native';
import {SQLite} from 'expo';
import Helper from '../../Common/Helper';
import Server from '../../Common/Server';

const db = SQLite.openDatabase('akun.db');

const DisposisiRow = ({ pengirim, tujuan, waktu, catatan, status_dibaca}) => (
    <View style={styles.list}>
        <TouchableOpacity>
            <View style={styles.container}>
                {(status_dibaca == null) ? <Image source={require('../../../assets/images/ic_suratmasuk_disposisi_nodibaca.png')} style={styles.icon}/> : <Image source={require('../../../assets/images/ic_suratmasuk_disposisi_yesdibaca.png')} style={styles.icon}/>}
                <View style={{flex: 1, marginLeft: 10}}>
                    <Text style={styles.title}>Pengirim Terusan</Text>
                    <Text style={styles.pengirim}>{pengirim}</Text>
                    <Text style={styles.title}>Penerima Terusan</Text>
                    <Text style={styles.tujuan}>{tujuan}</Text>
                    <Text style={styles.catatan}>{catatan}</Text>
                    <View style={{flex: 1, flexDirection: 'row', alignSelf:'flex-end', marginBottom: 5}}>
                        <Text style={styles.waktu}>{Helper.Date(waktu)}</Text>
                    </View>
                    
                </View>
                
            </View>
        </TouchableOpacity>
    </View>
);

const styles = StyleSheet.create({
    catatan: {
        flex: 1,
        backgroundColor: '#80808030',
        paddingTop: 3,
        paddingLeft: 3,
        marginBottom: 5
    },
    badge: {
        backgroundColor: 'rgb(23, 185, 120)',
        borderRadius: 10,
        width: 100,
        height: 18,
        alignItems: 'center',
        marginLeft: 5
    },
    list: {
        flex: 1,
        borderWidth: 1,
        borderBottomColor: '#80808030',
        borderTopColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        margin: 10,
    },
    container: {
        flex: 1,
        flexDirection: 'row',
    },
    read: {
        width: 24,
        height: 24,
        paddingBottom: 10,
        marginLeft: 5,
    },
    icon: {
        borderRadius: 25,
        backgroundColor: 'rgb(36,183,123)',
        width: 50,
        height: 50,
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        //color: '#80808080',
        fontSize: 16,
        fontWeight: 'bold'
    },
    pengirim: {
        color: 'rgb(255,87,51)',
        fontSize: 16
    },
    tujuan: {
        fontSize: 16
    },
    waktu: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#0000005f',
        alignSelf: 'center',
    },
    button: {
        backgroundColor: 'rgb(254, 89, 62)',
        borderRadius: 25,
        height: 50,
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',

    },
    btn_text: {
        color: 'white',
        fontWeight: 'bold',
    },
    footer: {
        height: 65,
        backgroundColor: 'white',
        justifyContent: 'center'
    },
})


const DisposisiListView = ({ itemList, reload }) => (
    <View style={{flex: 1, backgroundColor: 'rgb(242, 241, 242)'}}>
        
        <FlatList
            data={itemList}
            renderItem={({item}) => <DisposisiRow
                pengirim={item.pengirim_nama}
                tujuan={item.penerima_nama}
                waktu={item.waktu}
                status_dibaca={item.status_dibaca}
                catatan={item.catatan}
                reload={reload}
            />}
            keyExtractor={(item, index) => index.toString()}
        />
    </View>
);

export default class TeruskanKonsepList extends React.Component {
    render() {
        let {data, navigation, reload} = this.props;
        
        return (
            <View style={{flex: 1}}>
                <DisposisiListView 
                    itemList={data.list_disposisi}
                    reload={reload}
                />
                {data.can.mendisposisi ? (
                <View style={styles.footer}>
                    <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('TeruskanKonsep', {data: data.tujuan_disposisi, id: data.surat.id, reload: reload})}>
                        <View>
                            <Text style={styles.btn_text}>TERUSKAN KONSEP</Text>
                        </View>
                    </TouchableOpacity>
                </View>)
                : <View/>
                }
            </View>
            
        )
    }
}