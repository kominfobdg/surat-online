import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';



export default class Splash extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image
                    style = {{width: 150, height: 150}}
                    source={require('../../assets/images/email.png')}
                />
                <Text style={styles.title}>
                    SURAT ONLINE
                </Text>
                <Text style={styles.desc}>
                    Aplikasi Surat Online Pemkot Bandung
                </Text>
                <Text style={styles.desc}>
                    Versi 0.0.5
                </Text>
                <Text style={styles.footer}>
                    Diskominfo Bandung {'\u00A9'} 2019
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create ({
    container: { 
        backgroundColor: '#1976D2', 
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    title: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 10,
    },
    desc : {
        color: 'white',
    },
    footer: {
        color: 'black',
        position: 'absolute',
        bottom: 0,
        marginBottom: 10,
    }
});