import React from "react";
import {StyleSheet, View, Image, Text, TextInput, TouchableOpacity} from 'react-native'
import { SQLite } from 'expo';
import Home from './views/HomeScreen/Home';
import Server from "./views/Common/Server";
//import * as firebase from 'firebase';


const db = SQLite.openDatabase('akun.db');

export default class LoginScreen extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            home: false
        }

        db.transaction(
            tx => {
                tx.executeSql('DELETE FROM akun');
            }
        )

        // firebase.initializeApp({
        //     apiKey: 'AIzaSyCaTbaEN2MS8tsrUFIb33eBR3ZvBsry88U',
        //     authDomain: 'suratonlinebdg2019.firebaseapp.com',
        //     databaseURL: 'https://suratonlinebdg2019.firebaseio.com',
        //     //projectId: 'suratonlinebdg2019',
        //     storageBucket: 'suratonlinebdg2019.appspot.com',
        //     //583179841974
        // });
        
        
    }
    
    render() {
    const { navigate } = this.props
    if(this.state.home){
        return <Home />
    }
      return (
        <View style={styles.container}>
                <View style={styles.top}>
                    <Image
                        style={styles.logo}
                        source={require('./assets/images/email.png')}
                    />
                    <Text style={styles.title}>SURAT ONLINE</Text>
                    <Text style={styles.desc}>Aplikasi Surat Online Pemkot Bandung</Text>
                </View>
                <View style={styles.bot}>
                </View>
                <View style={styles.loginbox}>
                    <Text style={{textAlign: 'center', fontSize: 11}}>Silahkan masukkan alamat email Bandung.go.id dan kata sandi untuk melanjutkan ke dalam aplikasi SURAT ONLINE</Text>
                    <View style={styles.inputContainer}>
                        <Image
                            source={require('./assets/images/user.png')}
                            style={styles.icon}
                        />
                        <TextInput style={styles.input}
                            placeholder='email bandung.go.id'
                            onChangeText={(text) => this.setState({email: text})}
                            value={this.state.email}
                            autoCapitalize='none'
                            placeholderTextColor = '#808080'
                        ></TextInput>
                    </View>
                    <View style={styles.inputContainer}>
                        <Image
                            source={require('./assets/images/lockpassword.png')}
                            style={styles.icon}
                        />
                        <TextInput style={styles.input}
                            placeholder='kata sandi pengguna'
                            placeholderTextColor = '#808080'
                            onChangeText={(text) => this.setState({password: text})}
                            value={this.state.password}
                            autoCapitalize='none'
                            secureTextEntry={true}
                        ></TextInput>
                    </View>
                    <TouchableOpacity
                        style={{marginTop: 10}}
                        onPress={() => this.Login() }>
                        <View style={styles.buttonLogin}>
                            <Text style={{color: 'white', fontWeight: 'bold'}}>MASUK KE APLIKASI</Text>
                        </View>
                    </TouchableOpacity>
                    
                    <Text style={{color: 'grey', fontSize: 11, textAlign: 'center'}}>Tidak punya akun? Silahkan Hubungi admin Surat Online Diskominfo Bandung</Text>
                </View>
            </View>
      )
    }

    async Login(){
        
        Server.Login(this.state.email, this.state.password).then(
            (responseData) => {
                if(responseData.status == false){
                    alert(responseData.message);
                } else {
                    let data = responseData.data;
                    db.transaction(
                        tx => {
                            tx.executeSql('INSERT INTO akun(id, nama, nip, pangkat, telp, email, foto, token, id_role) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)', [data.id, data.nama, data.nip, data.diubah_pada, data.telp, data.email, data.foto, data.token_android, data.id_role]);
                            tx.executeSql('SELECT * FROM akun', [], (_, {rows}) => {
                                if(rows.length >= 1){
                                    this.setState({home: true})
                                } else {
                                    alert('Login gagal');
                                }
                            }); 
                        }
                    );
                }
            }
        )
        .catch((err) => { alert(err) });
    }

    

    
  }

  

  
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'lightgrey',
        flex: 1
    },
    top: {
        backgroundColor: '#1976D2',
        flex: 1,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        paddingTop: '10%',
    },
    bot: {
        flex: 1
    },
    loginbox: {
        backgroundColor: 'white',
        borderRadius: 5,
        borderColor: 'grey',
        borderWidth: 1,
        position: 'absolute',
        height: 300,
        width: '95%',
        alignSelf: 'center',
        top: '35%',
        paddingTop: 10,
        paddingHorizontal: 10,
    },
    logo: {
        width: 120,
        height: 120,
        alignSelf: 'center',
        paddingTop: 10,
    },
    title: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 16,
        alignSelf: 'center'
    },
    desc: {
        color: 'white',
        fontWeight: 'bold',
        alignSelf: 'center'
    },
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        borderColor: 'lightgrey',
        borderWidth: 1.5,
        width: '100%',
        height: 50,
        backgroundColor: '#f0f0f0',
        marginVertical: 10,
    },  
    input : {
        flex: 1,
        fontWeight: 'bold'
    },
    icon: {
        padding: 10,
        margin: 5,
        marginLeft: 10,
        height: 27,
        width: 22,
        resizeMode: 'stretch',
        alignItems: 'center'
    },
    buttonLogin: {
        backgroundColor: '#4CAF50',
        alignItems: 'center',
        borderRadius: 50,
        height: 50,
        width: 200,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    }
});