import React from "react";
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { SQLite } from 'expo'
import SplashScreen from './views/SplashScreen/SplashScreen'
import Home from './views/HomeScreen/Home'
import LoginScreen from './LoginScreen';
import Server from "./views/Common/Server";
import Disposisi from './views/SuratMasukScreen/Disposisi/Disposisi';
import ModelExample from './views/test';

const db = SQLite.openDatabase('akun.db');

class Screen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      timePassed: false,
      isLogged: 0 
    };

    db.transaction(
      tx => {
          tx.executeSql('SELECT * FROM akun', [], (_, {rows}) => {
              if(rows.length >= 1){
                let token = rows._array[0].token;
                console.log('token: ', token);
                Server.GetMasterData(token).then(
                  (responseData) => {
                    if(responseData.status == false){
                      alert(responseData.message);
                      this.setState({
                        isLogged: 1
                      });
                    } else {
                      this.setState({
                        isLogged: 2
                      });
                    }
                  }
                )
                .catch((err) => alert(err))
              } else {
                this.setState({
                  isLogged: 1
                });
              }
          }); 
      }
  )
    
  }
  render() {
    setTimeout(() => {this.setState({timePassed: true})}, 3000);
    if(this.state.timePassed){
      if(this.state.isLogged == 2){
        return <Home />
      } else if(this.state.isLogged == 1){
        return <LoginScreen />
      } else {
        return <SplashScreen />
      }
      
    } else {
      return <SplashScreen />
    }
   
  }
}


export default class App extends React.Component {
  
  componentDidMount() {
    db.transaction(tx => {
      tx.executeSql('CREATE TABLE IF NOT EXISTS akun('+
      'id integer primary key not null,'+
      'nama text,' +
      'nip text,' +
      'pangkat text,' +
      'telp text,' +
      'email text,' +
      'foto text,' +
      'token text,' +
      'id_role integer)');
      
     //tx.executeSql('DROP TABLE IF EXISTS akun');
    });
  }
  render(){
    return (
     <Screen />
     
    );
  }
}
